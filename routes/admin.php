<?php


Route::get('/dash', 'Dashboard\DashboardController@index');
Route::resource('/users', 'User\IndexController');
// Active , deActive accounts ( user ) :-
Route::get('/active_account/{id}', 'Active\ActiveAccountController@index');
Route::get('/deActive_account/{id}', 'Active\ActiveAccountController@drActive_account');

Route::resource('/categories', 'Category\IndexController');
Route::resource('/categories.subcategory', 'Category\SubCategoryController');


Route::resource('/notifications', 'Notification\IndexController');
Route::get('/userNotifiy/{id}', 'User\UserNotificationController@index');
Route::post('/userNotifiyStore', 'User\UserNotificationController@userNotifiyStore');

Route::resource('/settings', 'Setting\IndexController');//

//books
Route::resource('/books', 'Book\IndexController');//
Route::resource('/audio_books', 'AudioBook\IndexController');//
Route::resource('/books.comments', 'Book\CommentController');//
Route::resource('/books.rates', 'Book\RateController');//
Route::resource('/topics', 'Topic\IndexController');//
Route::resource('/articles', 'Article\IndexController');//
Route::resource('/articles.comments', 'Article\CommentsController');//

Route::resource('/openScreens', 'openScreen\IndexController');
Route::resource('/requested-books', 'Requestedbooks\IndexController');
Route::resource('/downloads', 'Download\IndexController');

Route::get('/userTopics/{id}', 'User\IndexController@topics');


Route::post('/getCategories', 'Ajax\IndexController@getCategories');
