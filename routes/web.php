<?php

use Illuminate\Support\Facades\Route;

Route::post('/getCategories', 'Admin\Ajax\IndexController@getCategories');
Route::get('/', 'Admin\Auth\LoginController@index');
Route::get('/admin', 'Admin\Auth\LoginController@index');
Route::post('/adminlogin', 'Admin\Auth\LoginController@adminlogin');
Route::post('/adminlogout', 'Admin\Auth\LoginController@adminlogout');

Route::get('/forget/{token}', 'Admin\Auth\LoginController@forget');
Route::post('/change-password', 'Admin\Auth\LoginController@changepassword');
Route::get('/password_updated_done/{id}', 'Admin\Auth\LoginController@password_updated_done');

Route::prefix('api')
    ->middleware(['api', 'lang'])
    ->group(function () {
        Route::post('register', [\App\Http\Controllers\Api\RegisterController::class, 'index']);
        Route::post('login', [\App\Http\Controllers\Api\LoginController::class, 'index']);
        Route::post('login-with-facebook', [\App\Http\Controllers\Api\LoginWithFacebookController::class, 'index']);
        Route::post('forget-password', [\App\Http\Controllers\Api\ForgetPasswordController::class, 'index']);
        Route::post('introduction-messages', [\App\Http\Controllers\Api\IntroductionMessagesController::class, 'index']);
        Route::post('topics', [\App\Http\Controllers\Api\TopicsController::class, 'index']);
        Route::post('books', [\App\Http\Controllers\Api\BooksController::class, 'index']);
        Route::post('audio-books', [\App\Http\Controllers\Api\AudioBooksController::class, 'index']);
        Route::post('book-details', [\App\Http\Controllers\Api\BookDetailsController::class, 'index']);
        Route::post('audio-book-details', [\App\Http\Controllers\Api\AudioBookDetailsController::class, 'index']);
        Route::post('book-comments', [\App\Http\Controllers\Api\BookCommentsController::class, 'index']);
        Route::post('our-picks', [\App\Http\Controllers\Api\OurPicksController::class, 'index']);
        Route::post('home-page-articles', [\App\Http\Controllers\Api\HomePageArticlesController::class, 'index']);
        Route::post('your-pen-page-articles', [\App\Http\Controllers\Api\YourPenPageArticlesController::class, 'index']);
        Route::post('article-details', [\App\Http\Controllers\Api\ArticleDetailsController::class, 'index']);
        Route::post('article-comments', [\App\Http\Controllers\Api\ArticleCommentsController::class, 'index']);
        Route::post('categories', [\App\Http\Controllers\Api\CategoriesController::class, 'index']);
        Route::post('sub-categories', [\App\Http\Controllers\Api\SubCategoriesController::class, 'index']);
        Route::post('request-book', [\App\Http\Controllers\Api\RequestBookController::class, 'index']);
        Route::post('most-viewed-books', [\App\Http\Controllers\Api\MostViewedBooksController::class, 'index']);
        Route::post('report-book', [\App\Http\Controllers\Api\ReportBookController::class, 'index']);
        Route::post('search-books', [\App\Http\Controllers\Api\SearchBooksController::class, 'index']);
        Route::post('user-profile', [\App\Http\Controllers\Api\UserProfileController::class, 'index']);
        Route::post('article-comment-replies', [\App\Http\Controllers\Api\ArticleCommentRepliesController::class, 'index']);
        Route::post('user-followings', [\App\Http\Controllers\Api\AllFollowingsController::class, 'index']);
        Route::post('user-followers', [\App\Http\Controllers\Api\AllFollowersController::class, 'index']);

        // Miss Password
        /*Route::post('/forgetPassword', 'ResetPassword\IndexController@index');*/
    });
