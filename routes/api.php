<?php

use Illuminate\Support\Facades\Route;

Route::post('update-personal-information', [\App\Http\Controllers\Api\UpdatePersonalInformationController::class, 'index']);
Route::post('profile', [\App\Http\Controllers\Api\ProfileController::class, 'index']);
Route::post('download', [\App\Http\Controllers\Api\DownloadController::class, 'index']);
Route::post('downloads', [\App\Http\Controllers\Api\DownloadsController::class, 'index']);
Route::post('remove-from-download', [\App\Http\Controllers\Api\RemoveFromDownloadsController::class, 'index']);
Route::post('followers', [\App\Http\Controllers\Api\FollowersController::class, 'index']);
Route::post('un-follow', [\App\Http\Controllers\Api\UnFollowController::class, 'index']);
Route::post('followings', [\App\Http\Controllers\Api\FollowingsController::class, 'index']);
Route::post('follow', [\App\Http\Controllers\Api\FollowController::class, 'index']);
Route::post('remove-from-favourites', [\App\Http\Controllers\Api\RemoveFromFavouritesController::class, 'index']);
Route::post('favourites', [\App\Http\Controllers\Api\FavouritesController::class, 'index']);
Route::post('add-to-favourites', [\App\Http\Controllers\Api\AddToFavouritesController::class, 'index']);
Route::post('like-book', [\App\Http\Controllers\Api\LikeBookController::class, 'index']);
Route::post('dislike-book', [\App\Http\Controllers\Api\DisLikeBookController::class, 'index']);
Route::post('share-book', [\App\Http\Controllers\Api\ShareBookController::class, 'index']);
Route::post('add-book-comment', [\App\Http\Controllers\Api\AddBookCommentController::class, 'index']);
Route::post('delete-book-comment', [\App\Http\Controllers\Api\DeleteBookCommentController::class, 'index']);
Route::post('update-book-comment', [\App\Http\Controllers\Api\UpdateBookCommentController::class, 'index']);
Route::post('download-book', [\App\Http\Controllers\Api\DownloadBookController::class, 'index']);
Route::post('like-article', [\App\Http\Controllers\Api\LikeArticleController::class, 'index']);
Route::post('dislike-article', [\App\Http\Controllers\Api\DisLikeArticleController::class, 'index']);
Route::post('share-article', [\App\Http\Controllers\Api\ShareArticleController::class, 'index']);
Route::post('my-topics', [\App\Http\Controllers\Api\MyTopicsController::class, 'index']);
Route::post('my-articles', [\App\Http\Controllers\Api\MyArticlesController::class, 'index']);
Route::post('add-article', [\App\Http\Controllers\Api\AddArticleController::class, 'index']);
Route::post('delete-article', [\App\Http\Controllers\Api\DeleteArticleController::class, 'index']);
Route::post('add-article-comment', [\App\Http\Controllers\Api\AddArticleCommentController::class, 'index']);
Route::post('update-article', [\App\Http\Controllers\Api\UpdateArticleController::class, 'index']);
Route::post('delete-article-comment', [\App\Http\Controllers\Api\DeleteArticleCommentController::class, 'index']);
Route::post('update-article-comment', [\App\Http\Controllers\Api\UpdateArticleCommentController::class, 'index']);
Route::post('user-profile', [\App\Http\Controllers\Api\UserProfileController::class, 'index']);
Route::post('change-password', [\App\Http\Controllers\Api\ChangePasswordController::class, 'index']);
Route::post('add-article-comment-reply', [\App\Http\Controllers\Api\AddArticleCommentReplyController::class, 'index']);
Route::post('update-article-comment-reply', [\App\Http\Controllers\Api\UpdateArticleCommentReplyController::class, 'index']);
Route::post('delete-article-comment-reply', [\App\Http\Controllers\Api\DeleteArticleCommentReplyController::class, 'index']);
