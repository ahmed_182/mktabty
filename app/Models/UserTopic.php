<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserTopic extends Model
{
    use HasFactory;

    protected $fillable = [
        "user_id",
        "topic_id",
    ];

    public function topic()
    {
        return $this->belongsTo(Topic::class, 'topic_id');
    }

    public function getDashTopicAttribute()
    {
        $att = null;
        if ($this->topic)
            $att = $this->topic->dash_name;
        return $att;
    }  public function getDashImageAttribute()
    {
        $att = null;
        if ($this->topic)
            $att = $this->topic->image;
        return $att;
    }
}
