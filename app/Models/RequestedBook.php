<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RequestedBook extends Model
{
    use HasFactory;
    protected $fillable =[
        'userName',
        'phone',
        'bookName',
        'author',
        'date',
        'status',
    ];

    public function getDashStatusNameAttribute()
    {
        if ($this->status == 1) {
            return trans("language.booknotavailable");
        } elseif($this->status == 2)
        {
            return trans("language.bookfound");
        }
        elseif($this->status == 3)
        {
            return trans("language.newbook");
        }
    }
}

