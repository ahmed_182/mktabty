<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookReport extends Model
{
    use HasFactory;
    protected $fillable =[
        "user_id",
        "book_id",
        "book_report_status_id",
    ];
}
