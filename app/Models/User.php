<?php

namespace App\Models;

use App\Http\Controllers\FilesHandler;
use App\ModulesConst\UserVerify;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use HasFactory, Notifiable, FilesHandler;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'username',
        'bio',
        'image',
        'facebookid',
        'token',
        'type',
        'fireBaseToken',
        'api_token',
        'remember_token',
        'passCode',
        'social_id',
        'loginBy_id',
        'email_verified_at',
        'user_type_id',
        'birthday',
    ];
    public function getmyArticleAttribute()
    {
        $item = Article::where("userId", $this->id)->count();
        return $item;
    }

    public function getmyfollowersAttribute()
    {
        $item = Follow::where("followerId", $this->id)->count();
        return $item;
    }

    public function getIsfollowAttribute()
    {
        $item = Follow::where("userId", $this->id)->count();
        return $item;
    }

    public function getDashNameAttribute()
    {
        $att = null;
        if ($this->name)
            $att = $this->name;
        return $att;
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'birthday' => 'datetime',
    ];

    public function setImageAttribute($value)
    {
        if ($value and is_file($value)) {
            $path = $this->storeFile($value);
            $this->attributes['image'] = strtolower($path);
        }
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function getDashStatusNameAttribute()
    {
        if ($this->userVerify == UserVerify::yes) {
            return trans("language.verfied");
        } else {
            return trans("language.not_verfied");
        }
    }

}
