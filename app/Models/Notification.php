<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;
    protected $fillable = [
        'title',
        'body',
    ];

    public function getTime()
    {
        $tim = \App\Notification::find($this->id)->created_at;
        $time = date("d M Y", strtotime($tim));
        return $time;
    }

    public function getAgoTime()
    {
        $tim = Notification::find($this->id)->created_at;
        $time = $tim->diffForHumans();
        return $time;
    }
}
