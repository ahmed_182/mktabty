<?php

namespace App\Models;

use App\Http\Controllers\FilesHandler;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IntroductionMessage extends Model
{
    use HasFactory ;

    protected $fillable = [
        'image',
        'title_ar',
        'title_en',
        'text_ar',
        'text_en',
    ];


    public function getServImageAttribute()
    {
        if ($this->image)
            return $attribute = $this->image;
        else
            return asset('assets/admin/images/logo.png');
    }

    public function getTitleAttribute()
    {
        if (app()->getLocale() == "ar")
            return $this->title_ar;
        if (app()->getLocale() == "en")
            return $this->title_en;
        return $this->title_en;
    }

    public function getTextAttribute()
    {
        if (app()->getLocale() == "ar")
            return $this->text_ar;
        if (app()->getLocale() == "en")
            return $this->text_en;
        return $this->text_ar;
    }
}
