<?php

namespace App\Models;

use App\Http\Controllers\FilesHandler;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory, FilesHandler;

    protected $fillable = [
        "image",
        "title",
        "body",
        "audio_url",
        "book_id",
        "userId",
        'no_likes',
        'no_dislikes',
        'no_comments',
        'no_views',
        'no_shares',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, "userId");
    }

    public function book()
    {
        return $this->belongsTo(Book::class, "book_id");
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, "articleId");
    }

  /*  public function setImageAttribute($value)
    {
        if ($value and is_file($value)) {
            $path = $this->storeFile($value);
            //dd($path);
            $this->attributes['image'] = url("storage/app/" . strtolower($path)) ;
        }
    }*/
    public function getDashBookAttribute()
    {
        $att = null;
        if ($this->book)
            $att = $this->book->dash_name;
        return $att;
    }
}
