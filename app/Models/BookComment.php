<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookComment extends Model
{
    use HasFactory;

    protected $fillable = [
        "book_id",
        "user_id",
        "comment",
        "rate",
    ];


    public function user()
    {
        return $this->belongsTo(User::class, "user_id");
    }

    public function getDashUserAttribute()
    {
        $att = null;
        if ($this->user)
            $att = $this->user->name;
        return $att;
    }
}
