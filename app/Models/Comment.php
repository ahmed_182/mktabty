<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;
    protected $table = 'comments';

    protected $fillable = [
        "body",
        "userId",
        "articleId",
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'userId');
    }
}
