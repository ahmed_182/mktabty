<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Download extends Model
{
    use HasFactory;
    protected $fillable = [
        "userId",
        "bookId",
    ];

    public function book()
    {
        return $this->belongsTo(Book::class, "bookId");
    }

    public function user()
    {
        return $this->belongsTo(User::class, "userId");
    }



    //dashboard

    public function getDashUserAttribute()
    {
        $att = null;
        if ($this->user)
            $att = $this->user->name;
        return $att;
    }

    public function getDashBookNameAttribute()
    {
        $att = null;
        if ($this->book)
            $att = $this->book->bookName;
        return $att;
    }
}
