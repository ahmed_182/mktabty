<?php

namespace App\Models;

use App\Http\Controllers\FilesHandler;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory , FilesHandler;

    protected $fillable = [
        'fileTypeId',
        'bookName',
        'bookDescription',
        'categoryId',
        'subCategoryId',
        'autherName',
        'imgURL',
        'bookURL',
        'our_pick',
        'no_views',
        'no_shares',
        'no_comments',
        'no_likes',
        'no_dislikes',
    ];
    public function getDashNameAttribute()
    {
        $att = null;
        if ($this->bookName)
            $att = $this->bookName;
        return $att;
    }

    public function getBookSizeAttribute($request)
    {
        // todo
        return $this->size . " MB";
    }

    public function category()
    {
        return $this->belongsTo(Category::class, "categoryId");
    }

    public function sub_category()
    {
        return $this->belongsTo(SubCategory::class , "subCategoryId");
    }
    public function book_comments()
    {
        return $this->hasMany(BookComment::class);
    }

    public function getDashcategoryAttribute()
    {
        $attribute = trans('language.notSelected');
        if ($this->category)
            $attribute = $this->category->categoryName;
        return $attribute;
    }

    public function getDashSubCategoryAttribute()
    {
        $attribute = trans('language.notSelected');
        if ($this->sub_category)
            $attribute = $this->sub_category->subCategoryName;
        return $attribute;
    }
}
