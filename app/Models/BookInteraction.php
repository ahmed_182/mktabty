<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookInteraction extends Model
{
    use HasFactory;
    const typeLike = 1;
    const typeDisLike = 2;
    const typeShare = 3;
    protected $fillable = [
        'user_id',
        'book_id',
        'type',
    ];
}
