<?php

namespace App\Http\Controllers;

use App\Mail\resetPassworMail;
use App\Models\Notification;
use App\Models\User;
use App\Models\UserNotification;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Mail;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests , ResponseHandler;


    public function notificationHandler($title, $body, $users_ids, $notiType = null, $notiTypeId = null)
    {
        $data["title"] = $title;
        $data["body"] = $body;
        $notification = Notification::create($data);
        foreach ($users_ids as $user_id) {
            $user = User::find($user_id);
            // dd($user);
            $data["user_id"] = $user_id;
            $data["notification_id"] = $notification->id;
            $data["user_id"] = $user->id;
            UserNotification::create($data);
            if ($user->fire_base_token) {
                $this->push_notification([$user->fire_base_token], $data);
            }
        }
    }

    public function reset_Password($request, $user)
    {
        // todo Send Email Code ( passCode To reset_Password User Email ) ...
        Mail::to($user->email)->send(new resetPassworMail($user));
    }

}
