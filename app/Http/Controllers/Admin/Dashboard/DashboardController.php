<?php

    namespace App\Http\Controllers\Admin\Dashboard;


    use App\Models\RequestedBook;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class DashboardController extends Controller
    {

        public function index()
        {
           // dd("gyhdf");
            $orders = RequestedBook::latest()->take(5)->get();
            return view('admin.dashboard.index' ,compact('orders'));
        }
    }
