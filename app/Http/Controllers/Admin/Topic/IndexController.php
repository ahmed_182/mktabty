<?php

namespace App\Http\Controllers\Admin\Topic;

use App\Ministry;
use App\Models\Topic;
use App\Open_screen;
use App\Traits\storeImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{

    use storeImage;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = Topic::query();
        $items = $this->filter($request, $items);

        return view('admin.topics.index', compact('items'));
    }
    public function filter($request, $items)
    {
        if ($request->name) {
            $items = $items->where("name_ar", 'LIKE', '%' . $request->name . '%')->orWhere("name_en", 'LIKE', '%' . $request->name . '%');
        }
        $items = $items->orderBy("id", "desc")->paginate(10);
        return $items;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.topics.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'image' => 'required',
            'name_ar' => 'required',
            'name_en' => 'required',
        ]);
        if ($request->image) {
            $data['image'] = $this->storeImage($request->image);
        }
        Topic::create($data);
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/topics'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Topic::findOrFail($id);
        return view('admin.topics.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Topic::findOrFail($id);
        $data = $request->validate([
            'image' => '',
            'name_ar' => '',
            'name_en' => '',
        ]);
        if ($request->image) {
            $data['image'] = $this->storeImage($request->image);
        }
        $item->update($data);
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/topics'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Topic::findOrFail($id)->delete();
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/topics'));
    }
}
