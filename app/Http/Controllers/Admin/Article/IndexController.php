<?php

    namespace App\Http\Controllers\Admin\Article;

    use App\Models\Article;
    use App\Models\Book;
    use App\Models\Category;
   ;
    use App\Traits\storeImage;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        use storeImage;


        public function index(Request $request)
        {
            $items = Article::query();
            $items = $this->filter($request, $items);
            return view('admin.articles.index', compact('items'));

        }

        public function filter($request, $items)
        {
            if ($request->title) {
                $items = $items->where("title", 'LIKE', '%' . $request->title . '%');
            }
            if ($request->book_id) {
                $items = $items->where("book_id", $request->book_id);
            }
            $items = $items->orderBy("id", "desc")->paginate(10);
            return $items;
        }


        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()

        {

            return view('admin.articles.create');
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            $data = $request->all();
            if ($request->image) {
                $data['image'] = $this->storeImage($request->image);
            }
           // dd($data);
            $book = Article::create($data);


            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/articles'));
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {/*
        $items = Categories::findOrFail($id);
        return view('admin.Categories.show',compact('items'));*/
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $item = Article::findOrFail($id);
            return view('admin.articles.edit', compact('item'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $item = Article::findOrFail($id);
            $data = $request->all();
            if ($request->imgURL) {
                $data['imgURL'] = $this->storeImage($request->imgURL);
            }
            $item->update($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/articles'));
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $item = Article::findOrFail($id)->delete();
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/articles'));
        }
    }
