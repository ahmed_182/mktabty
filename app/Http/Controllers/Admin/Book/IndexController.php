<?php

    namespace App\Http\Controllers\Admin\Book;

    use App\Models\Book;
    use App\Models\Category;
   ;

    use App\Traits\storeFile;
    use App\Traits\storeImage;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        use storeImage ,storeFile;


        public function index(Request $request)
        {
            $items = Book::where("fileTypeId", 1);
            $items = $this->filter($request, $items);
            return view('admin.books.index', compact('items'));

        }

        public function filter($request, $items)
        {
            if ($request->bookName) {
                $items = $items->where("bookName", 'LIKE', '%' . $request->bookName . '%');
            }
            if ($request->category_id) {
                $items = $items->where("categoryId", $request->category_id);
            }

            if ($request->subCategoryId) {
                $items = $items->where("subCategoryId", $request->subCategoryId);
            }
            $items = $items->orderBy("id", "desc")->paginate(10);
            return $items;
        }


        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()

        {

            return view('admin.books.create');
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            $data = $request->all();
            if ($request->imgURL) {
                $data['imgURL'] = $this->storeImage($request->imgURL);
            }
            if ($request->bookURL) {
                $data['bookURL'] = $this->storeFile($request->bookURL);
            }
            $data['fileTypeId'] = 1;
           // dd($data);
            $book = Book::create($data);


            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/books'));
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {/*
        $items = Categories::findOrFail($id);
        return view('admin.Categories.show',compact('items'));*/
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $item = Book::findOrFail($id);
            return view('admin.books.edit', compact('item'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $item = Book::findOrFail($id);
            $data = $request->all();
            if ($request->imgURL) {
                $data['imgURL'] = $this->storeImage($request->imgURL);
            }
            $item->update($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/books'));
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $item = Book::findOrFail($id)->delete();
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/books'));
        }
    }
