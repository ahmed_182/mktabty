<?php

namespace App\Http\Controllers\Admin\openScreen;

use App\Country;
use App\Models\IntroductionMessage;
use App\Open_screen;
use App\Traits\storeImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{

    use storeImage;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = IntroductionMessage::query();
        $items = $this->filter($request, $items);
        return view('admin.openScreens.index', compact('items'));
    }
    public function filter($request, $items)
    {
        if ($request->title) {
            $items = $items->where("title_ar", 'LIKE', '%' . $request->title . '%')->orWhere("title_en", 'LIKE', '%' . $request->title . '%');
        }
        $items = $items->orderBy("id", "desc")->paginate(10);
        return $items;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.openScreens.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'image' => 'required',
            'title_ar' => 'required',
            'title_en' => 'required',
            'text_ar' => 'required',
            'text_en' => 'required',
        ]);
        if ($request->image) {
            $data['image'] = $this->storeImage($request->image);
        }
        IntroductionMessage::create($data);
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/openScreens'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = IntroductionMessage::findOrFail($id);
        return view('admin.openScreens.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = IntroductionMessage::findOrFail($id);
        $data = $request->validate([
            'image' => '',
            'title_ar' => '',
            'title_en' => '',
            'text_ar' => '',
            'text_en' => '',
        ]);
        if ($request->image) {
            $data['image'] = $this->storeImage($request->image);
        }
        $item->update($data);
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/openScreens'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        IntroductionMessage::findOrFail($id)->delete();
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/openScreens'));
    }
}
