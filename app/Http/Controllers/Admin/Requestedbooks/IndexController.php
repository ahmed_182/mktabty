<?php

namespace App\Http\Controllers\Admin\Requestedbooks;

use App\Contact;
use App\Contact_us;
use App\Models\RequestedBook;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
            $items = RequestedBook::query();
            $items = $this->filter($request, $items);
        //dd($items);
            return view('admin.requested_books.index', compact('items'));
    }

    public function filter($request, $items)
    {

        if ($request->userName)
            $items = $items->where("userName", 'LIKE', '%' . $request->userName . '%');
        if ($request->phone) {
            $items = $items->where("phone", 'LIKE', '%' . $request->phone . '%');
        }
        if ($request->bookName) {
            $items = $items->where("bookName", 'LIKE', '%' . $request->bookName . '%');
        }
        $items = $items->orderBy("id", "desc")->paginate(10);
        return $items;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = RequestedBook::findOrFail($id);

        return view('admin.requested_books.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = RequestedBook::findOrFail($id);
        $data = $request->validate([
            'status' => 'required',
        ]);
        $data['status'] = $request->status;
        $item->update($data);

        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/requested-books'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = RequestedBook::findOrFail($id)->delete();
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/requested-books'));
    }
}
