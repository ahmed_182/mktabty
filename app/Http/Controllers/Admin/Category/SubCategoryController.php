<?php

    namespace App\Http\Controllers\Admin\Category;

    use App\Categories;
    use App\Models\Category;
    use App\Models\SubCategory;
    use App\Product;
    use App\Store_his_categories;
    use App\Traits\storeImage;
    use App\User;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class SubCategoryController extends Controller
    {
        use storeImage;

        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index($category_id)
        {
            $items = SubCategory::where('categoryId', $category_id)->orderBy("id", "desc")->paginate(10);
            return view('admin.categories.subcategory.index', compact('category_id', 'items'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create($category_id)
        {
            //dd($category_id);
            $category = Category::findorfail($category_id);
            //dd($category);
            return view('admin.categories.subcategory.create', compact('category'));
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request, $category_id)
        {
            $data = $request->validate([
                'subCategoryName' => 'required',
            ]);

            $data["categoryId"] = $category_id;
            $item = SubCategory::create($data);
            session()->flash('success', trans('language.done'));
            return redirect("admin/categories/$category_id/subcategory");
        }


        /**
         * Display the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */


        public function edit($category_id, $subcategory_id)
        {
            $category = Category::find($category_id);
            $item = SubCategory::find($subcategory_id);
            return view('admin.categories.subcategory.edit', compact('category',  'item'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $category_id, $subcategory_id)
        {
            $data = $request->validate([
                'subCategoryName' => '',
            ]);
            $item = SubCategory::find($subcategory_id);
            dd($item);
            $item->update($data);
            session()->flash('success', trans('language.done'));
            return redirect("admin/categories/$category_id/subcategory");

        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($cat, $id)
        {
            $category = SubCategory::findOrFail($id);
            $category->delete();
            session()->flash('success', trans('language.done'));
            return back();
        }
    }
