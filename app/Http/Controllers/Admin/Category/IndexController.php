<?php

    namespace App\Http\Controllers\Admin\Category;

    use App\Models\Category;
   ;
    use App\Traits\storeImage;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        use storeImage;


        public function index(Request $request)
        {
            $items = Category::query();
            $items = $this->filter($request, $items);
            return view('admin.categories.index', compact('items'));

        }

        public function filter($request, $items)
        {
            if ($request->categoryName) {
                $items = $items->where("categoryName", 'LIKE', '%' . $request->categoryName . '%');
            }
            $items = $items->orderBy("id", "desc")->paginate(10);
            return $items;
        }


        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()

        {

            return view('admin.Categories.create');
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            $data = $request->validate([
                'categoryName' => 'required',
            ]);

            $category = Category::create($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/categories'));
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {/*
        $items = Categories::findOrFail($id);
        return view('admin.Categories.show',compact('items'));*/
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $item = Category::findOrFail($id);
            return view('admin.categories.edit', compact('item'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $item = Category::findOrFail($id);
            $data = $request->validate([
                'categoryName' => '',
            ]);

            $item->update($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/categories'));
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $item = Category::findOrFail($id)->delete();
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/categories'));
        }
    }
