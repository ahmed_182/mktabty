<?php

    namespace App\Http\Controllers\Admin\Category;

    use App\Categories;
    use App\Traits\storeImage;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class SubSubCategoryController extends Controller
    {
        use storeImage;

        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            //
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create($cat, $category_id)
        {
            $category = Categories::findorfail($category_id);
            return view('admin.categories.subsubcategory.create', compact('category_id', 'category'));
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request, $category_id, $subcategory)
        {
            $data = $request->validate([

                'name_ar' => 'required',
                'name_en' => 'required',
                'image' => 'mimes:jpeg,jpg,png,gif|required|max:2048',
            ]);
            if ($data['image']) {
                $data['image'] = $this->storeImage($data['image']);
            }
            $data["parent_id"] = $subcategory;
            $item = Categories::create($data);
            session()->flash('success', trans('language.done'));
            return redirect("admin/categories/$category_id/subcategory");
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            //
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            //
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($cat, $id)
        {
            $category = Categories::findOrFail($id);
            $category->delete();
            Categories::where('parent_id', $id)->delete();
            session()->flash('success', trans('language.done'));
            return redirect(url("/admin/categories/$cat/subcategory"));
        }
    }
