<?php

namespace App\Http\Controllers\Admin\Notification;


use App\City;
use App\Country;
use App\District;
use App\Driver;
use App\Driver_districts;
use App\ModulesConst\UserOnlineStatus;
use App\ModulesConst\UserPaidTyps;
use App\ModulesConst\UserTyps;
use App\Models\Notification;
use App\Models\User;
use App\Models\UserNotification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index()
    {
        $items = Notification::orderBy('id', 'desc')->paginate(10);
        return view('admin.notifications.index', compact('items'));
    }
    public function create()
    {
        return view('admin.notifications.create');

    }


    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|string',
            'body' => 'required|string',
        ]);
        $this->clientsTypeHandler($request);
        //dd($request);
        session()->flash('success', trans('language.SendNotifMessage'));
        return back();
    }

    public function clientsTypeHandler($request)
    {
        //onlineClients
        if ($request->client_type == "users") {
            $users = User::where('user_type_id', UserTyps::user)->orderBy('id', 'desc')->pluck('id');
            $this->clientsHandler($users, $request);
        }

        if ($request->client_type == "drivers") {
            $drivers = User::where('user_type_id', UserTyps::drivers)->orderBy('id', 'desc')->pluck('id');
            $this->clientsHandler($drivers, $request);
        }

        if ($request->client_type == "alluser") {
            $alluser = User::orderBy('id', 'desc')->pluck('id');
            $this->clientsHandler($alluser, $request);

        }

    }

    public function clientsHandler($users, $request)
    {
        $users_ids = $users;
        $tokens = User::whereIn("id", $users_ids)
            ->where('fireBaseToken', "!=", null)
            ->pluck('fireBaseToken')->toArray();

        if (count($tokens) > 0) {
            $this->push_notification($tokens, $request->all());
        }
        $this->mySqlHandler($request, $users_ids);

    }


    public function mySqlHandler($request, $users_ids)
    {
        $notification = Notification::create($request->all());
        //dd( $notification);
        foreach ($users_ids as $user_id) {
            $data["user_id"] = $user_id;
            $data["notification_id"] = $notification->id;
            UserNotification::create($data);
        }
    }
}
