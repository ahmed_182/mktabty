<?php

    namespace App\Http\Controllers\Admin\Ajax;

    use App\Categories;
    use App\Category;
    use App\Models\SubCategory;
    use App\Sub_category;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class Indexcontroller extends Controller
    {

        public function getCategories(Request $request)
        {
            $sub_types = SubCategory::where("categoryId", $request->category_id)->orderBy("id", "desc")->get();
            $data['sub_types'] = $sub_types;
            if (count($sub_types) > 0) {
                $data["status"] = true;
            } else {
                $data["status"] = false;
            }
            return response()->json($data);
        }

    }
