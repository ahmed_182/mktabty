<?php

namespace App\Http\Controllers\Admin\User;

use App\Models\Notification;
use App\Models\User;
use App\Models\User_notification;
use App\Models\UserNotification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserNotificationController extends Controller
{
    public function index($id, Request $request)
    {
        $client = User::find($id);
        return view('admin.users.notification.index', compact('client'));
    }

    public function userNotifiyStore(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'body' => 'required',
        ]);
        $users = User::where('id', $request->client_id)->pluck('id');
        //dd($users);
        $this->clientsHandler($users, $request);
        session()->flash('success', trans('language.SendNotifMessage'));
        return back();
    }

    public function clientsHandler($users, $request)
    {

        $users_ids = $users;
        $tokens = User::whereIn("id", $users_ids)
            ->where('fireBaseToken', "!=", null)
            ->pluck('fireBaseToken')->toArray();
        if (count($tokens) > 0) {
            $this->fireBaseNotificationsHandler($tokens, $request->all());
            $this->mySqlHandler($request, $users_ids);
        }
    }


    public function mySqlHandler($request, $users_ids)
    {
        $notification = Notification::create($request->all());
        foreach ($users_ids as $user_id) {
            $data["user_id"] = $user_id;
            $data["notification_id"] = $notification->id;
            UserNotification::create($data);
        }
    }
}
