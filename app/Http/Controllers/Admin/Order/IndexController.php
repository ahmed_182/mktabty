<?php

    namespace App\Http\Controllers\Admin\Order;

    use App\City;
    use App\Country;
    use App\Open_screen;
    use App\Order;
    use App\Order_extra_products;
    use App\Order_products;
    use App\Order_status;
    use App\Order_status_log;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        public function index()
        {
            $items = Order::orderBy('id', 'desc')->paginate(10);
            return view('admin.orders.index', compact('items'));
        }

        public function destroy($id)
        {
            Order::findOrFail($id)->delete();
            Order_products::where('order_id', $id)->delete();
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/orders'));
        }

        public function order_products($order_id)
        {
            $items = Order_products::where('order_id', $order_id)->orderBy('id', 'desc')->paginate(10);
            return view('admin.orders.products', compact('items'));
        }

        public function order_extra_products($order_id)
        {
            $items = Order_extra_products::where('order_id', $order_id)->orderBy('id', 'desc')->paginate(10);
            return view('admin.orders.extra_products', compact('items'));
        }

        public function statues($id)
        {
            $item = Order::findOrFail($id);
            $order_staids = Order_status_log::where('order_id', $item->id)->pluck('order_status_id');
            $items = Order_status::whereNotIn('id', $order_staids)->get();
            $order_status = Order_status::whereIn('id', $order_staids)->orderBy('id', 'desc')->get();

            return view('admin.orders.statueshome', compact('item', 'items', 'order_status'));
        }

        public function statuesStore($id, Request $request)
        {
            $data = $request->validate([
                'order_status_id' => 'required',
            ]);

            $data['order_id'] = $id;
            $item = Order::findOrFail($id);
            $item->update($data);
            // add this status in log :-
            Order_status_log::create($data);
            $body = " تم تغيير حالة الطلب " . " ( " . $item->ServOrderStatues . " ) ";
            $this->notificationHandler(trans("language.appName"), $body, [$item->user_id], null, null);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/orders'));
        }


    }
