<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\ModulesConst\UserVerify;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class LoginController extends Controller
{
    public function index()
    {
        if (Auth::check())
            return redirect('/admin/dash');
        else
            return view('auth.login');
    }

    public function forget($token)
    {
        $item = User::where("api_token", $token)->first();
        if (!$item) {
            return view('auth.passwords.not_auth');
        }
        return view('auth.passwords.reset', compact('item'));
    }

    public function changepassword(Request $request)
    {
        $request->validate([
            'user_token' => ['required', 'string'],
            'password' => ['required', 'string'],
        ]);


        $item = User::where("api_token", $request->user_token)->first();
        if (!$item) {
            return view('auth.passwords.not_auth');
        }

        if ($request->password) {
            $data['password'] = Hash::make($request->password);
        }
        $item->update($data);
        session()->flash('success', trans('language.done'));
        return redirect("/password_updated_done/$item->id");
    }

    public function password_updated_done($id)
    {
        $item = User::find($id);
        return view('auth.passwords.password_updated_done', compact("item"));
    }

    public function adminlogin(Request $request)
    {
        $request->validate([
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:6'],
        ]);
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'user_type_id' => 1])) {
            // The user is active, not suspended, and exists.
            $user = Auth::user();
            Auth::login($user);
            return redirect('/admin/dash');
        } else {
            session()->flash('danger', trans('language.loginError'));
            return back();
        }
    }

    public function adminlogout(Request $request)
    {
        Auth::logout();
        // regenrate session :
        $request->session()->regenerate();
        // then redirect to home page  :
        return redirect('/');
    }

    public function verfiyEmail($id, $code)
    {
        $user = User::find($id);
        if ($user->passCode == $code) {
            $user->email_verified = 1;
            $user->passCode = null;
            $user->save();
            return redirect('/thanks_page');
        }
    }
}
