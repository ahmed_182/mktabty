<?php

namespace App\Http\Controllers\Admin\Download;

use App\Http\Controllers\Controller;
use App\Models\Download;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = Download::query();
        $items = $this->filter($request, $items);
        //dd($items);
        return view('admin.downloads.index', compact('items'));
    }

    public function filter($request, $items)
    {

        if ($request->userName)
            $items = $items->where("userName", 'LIKE', '%' . $request->userName . '%');
        if ($request->phone) {
            $items = $items->where("phone", 'LIKE', '%' . $request->phone . '%');
        }
        if ($request->bookName) {
            $items = $items->where("bookName", 'LIKE', '%' . $request->bookName . '%');
        }
        $items = $items->orderBy("id", "desc")->paginate(10);
        return $items;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Download::findOrFail($id)->delete();
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/downloads'));
    }
}
