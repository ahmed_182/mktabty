<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ShareBookRequest;
use App\Models\Book;
use App\Models\BookInteraction;

class ShareBookController extends Controller
{
    public function index(ShareBookRequest  $request)
    {
        $data = $request->validated();
        $data["user_id"] = auth()->user()->id;
        $data["type"] = BookInteraction::typeShare;
        BookInteraction::create($data);
        Book::findOrFail($data["book_id"])->increment("no_shares");
        return $this->response(null);
    }
}
