<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\DeleteBookCommentRequest;
use App\Http\Requests\UpdateBookCommentRequest;
use App\Models\BookComment;

class UpdateBookCommentController extends Controller
{
    public function index(UpdateBookCommentRequest $request)
    {
        // todo check user
        $data = $request->validated();
        BookComment::findOrFail($request->comment_id)->update($data);
        return $this->response(null);
    }
}
