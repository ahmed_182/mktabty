<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\RequestBookRequest;
use App\Models\RequestedBook;

class RequestBookController extends Controller
{
    public function index(RequestBookRequest $request)
    {
        $data = $request->validated();
        $data['date'] = time();
        RequestedBook::create($data);
        return $this->response(null);
    }
}
