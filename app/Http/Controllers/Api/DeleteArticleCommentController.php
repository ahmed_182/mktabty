<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\DeleteArticleCommentReplyRequest;
use App\Http\Requests\DeleteArticleCommentRequest;
use App\Models\Comment;
use App\Models\CommentReply;

class DeleteArticleCommentController extends Controller
{
    public function index(DeleteArticleCommentRequest $request)
    {
        $data = Comment::findOrFail($request->comment_id);
        $data->delete();
        return $this->response(null);
    }
}
