<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\FollowersRequest;
use App\Http\Resources\MyFollowersResource;
use App\Models\Follow;

class AllFollowersController extends Controller
{
    public function index(FollowersRequest $request)
    {
        $data = Follow::with(["user"])
            ->where("followerId", $request->user_id)
            ->paginate(10);
        return $this->response(MyFollowersResource::collection($data));
    }
}
