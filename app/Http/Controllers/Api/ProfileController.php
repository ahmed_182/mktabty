<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileRequest;
use App\Http\Resources\ProfileResource;

class ProfileController extends Controller
{
    public function index(ProfileRequest $request)
    {
        return $this->response(ProfileResource::make(auth()->user()));
    }
}
