<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddArticleCommentReplyRequest;
use App\Models\CommentReply;

class AddArticleCommentReplyController extends Controller
{
    public function index(AddArticleCommentReplyRequest $request)
    {
        $data = $request->validated();
        $data["user_id"] = auth()->user()->id;
        CommentReply::create($data);
        return $this->response(null);
    }
}
