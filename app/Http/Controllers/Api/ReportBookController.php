<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ReportBookRequest;
use App\Models\BookReport;

class ReportBookController extends Controller
{
    public function index(ReportBookRequest  $request)
    {
        $data = $request->validated();
        $data["user_id"] = auth()->user()->id;
        BookReport::create($data);
        return $this->response(null);
    }
}
