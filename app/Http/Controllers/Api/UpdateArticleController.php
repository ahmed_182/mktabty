<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateArticleRequest;
use App\Models\Article;

class UpdateArticleController extends Controller
{
    public function index(UpdateArticleRequest  $request)
    {
        // todo check user
        $data = $request->validated();
        Article::findOrFail($request->article_id)->update($data);
        return $this->response(null);
    }
}
