<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Traits\apiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;


class ForgetPasswordController extends Controller
{

    use apiResponse;

    public function index(Request $request)
    {

        $data = $request->validate([
            'email' => 'required',
        ]);


        $user = User::where('email', $request->email)->first();
        if (!$user) {
            return $this->apiResponse($request, trans('language.not_Existemail'), null, false, 400);
        }

        Auth::login($user);
        $this->reset_Password($data, $user);
        return $this->sendResponse($request, 'done', null, true, $user->api_token);
    }

}
