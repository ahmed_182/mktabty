<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordRequest;

class ChangePasswordController extends Controller
{
    public function index(ChangePasswordRequest $request)
    {
        $data = $request->validated();
        $user = auth()->user();
        $user->update($data);
        return $this->response(null);
    }
}
