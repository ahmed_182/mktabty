<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddArticleCommentRequest;
use App\Http\Requests\UpdateArticleCommentRequest;
use App\Models\Article;
use App\Models\BookComment;
use App\Models\Comment;

class UpdateArticleCommentController extends Controller
{
    public function index(UpdateArticleCommentRequest $request)
    {
        // todo check user
        $data = $request->validated();
        Comment::findOrFail($request->comment_id)->update($data);
        return $this->response(null);
    }
}
