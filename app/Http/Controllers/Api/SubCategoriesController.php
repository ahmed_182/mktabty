<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\SubCategoriesRequest;
use App\Http\Resources\SubCategoriesResource;
use App\Models\SubCategory;

class SubCategoriesController extends Controller
{
    public function index(SubCategoriesRequest $request)
    {
        $query = SubCategory::query();
        if ($request->categoryId)
            $query->where("categoryId", $request->categoryId);
        $data = $query->paginate(10);
        return $this->response(SubCategoriesResource::collection($data));
    }
}
