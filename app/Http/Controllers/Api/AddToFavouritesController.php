<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddToFavouritesRequest;
use App\Models\Favorite;

class AddToFavouritesController extends Controller
{
    public function index(AddToFavouritesRequest $request)
    {
        $data = $request->validated();
        $data["userId"] = auth()->user()->id;
        Favorite::create($data);
        return $this->response(null);
    }
}
