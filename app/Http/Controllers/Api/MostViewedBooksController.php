<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\MostViewedBooksRequest;
use App\Http\Resources\MostViewedBooksResource;
use App\Models\Book;

class MostViewedBooksController extends Controller
{
    public function index(MostViewedBooksRequest $request)
    {
        $query = Book::select("id", "imgURL", "bookName","no_views")
            ->where("fileTypeId", 1)
            ->whereNotNull("no_views");
        if ($request->categoryId)
            $query->where("categoryId", $request->categoryId);
        if ($request->subCategoryId)
            $query->where("subCategoryId", $request->subCategoryId);
        $query->orderByDesc('no_views');
        $data = $query->paginate(10);
        return $this->response(MostViewedBooksResource::collection($data));
    }
}
