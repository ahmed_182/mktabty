<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\OurPicksRequest;
use App\Http\Resources\OurPicksResource;
use App\Models\Book;

class OurPicksController extends Controller
{
    public function index(OurPicksRequest $request)
    {
        $data = Book::select("id", "imgURL", "bookName")
            ->where("fileTypeId", 1)
            ->where("our_pick", 1)
            ->paginate(10);
        return $this->response(OurPicksResource::collection($data));
    }
}
