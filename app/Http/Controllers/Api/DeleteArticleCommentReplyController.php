<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\DeleteArticleCommentReplyRequest;
use App\Models\CommentReply;

class DeleteArticleCommentReplyController extends Controller
{
    public function index(DeleteArticleCommentReplyRequest $request)
    {
        $data = CommentReply::findOrFail($request->comment_reply_id);
        $data->delete();
        return $this->response(null);
    }
}
