<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddArticleCommentRequest;
use App\Models\Article;
use App\Models\Comment;

class AddArticleCommentController extends Controller
{
    public function index(AddArticleCommentRequest $request)
    {
        $data = $request->validated();
        $data["userId"] = auth()->user()->id;
        Comment::create($data);
        Article::findOrFail($data["articleId"])->increment("no_comments");
        return $this->response(null);
    }
}
