<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddArticleRequest;
use App\Models\Article;
use App\Http\Controllers\FilesHandler;


class AddArticleController extends Controller
{
    use FilesHandler;
    public function index(AddArticleRequest  $request)
    {
        $data = $request->validated();
        $data["userId"] = auth()->user()->id;
        if ($request->image) {
            $data['image'] = $this->storeFile($request->image);
        }
        Article::create($data);
        return $this->response(null);
    }
}
