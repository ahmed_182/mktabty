<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\FollowingsRequest;
use App\Http\Resources\FollowingResource;
use App\Models\Follow;

class AllFollowingsController extends Controller
{
    public function index(FollowingsRequest $request)
    {
        $data = Follow::with(["follower"])
            ->where("userId", $request->user_id)
            ->paginate(10);
        return $this->response(FollowingResource::collection($data));
    }
}
