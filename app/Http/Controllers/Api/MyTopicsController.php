<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\MyTopicsRequest;
use App\Http\Resources\TopicsResource;
use App\Models\UserTopic;

class MyTopicsController extends Controller
{
    public function index(MyTopicsRequest $request)
    {
        $data = UserTopic::where("user_id", auth()->user()->id)->paginate(10);
        return $this->response(TopicsResource::collection($data));
    }
}
