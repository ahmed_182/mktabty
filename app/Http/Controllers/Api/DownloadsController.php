<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\DownloadsRequest;
use App\Http\Resources\DownloadsResource;
use App\Models\Download;

class DownloadsController extends Controller
{
    public function index(DownloadsRequest $request)
    {
        $data = Download::where("userId", auth()->user()->id)
            ->with("book")
            ->paginate(10);
        return $this->response(DownloadsResource::collection($data));
    }
}
