<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LikeBookRequest;
use App\Models\Book;
use App\Models\BookInteraction;

class LikeBookController extends Controller
{
    public function index(LikeBookRequest  $request)
    {
        $data = $request->validated();
        $data["user_id"] = auth()->user()->id;
        $data["type"] = BookInteraction::typeLike;
        BookInteraction::create($data);
        Book::findOrFail($data["book_id"])->increment("no_likes");
        return $this->response(null);
    }
}
