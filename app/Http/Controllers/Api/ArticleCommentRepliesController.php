<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleCommentRepliesRequest;
use App\Http\Resources\ArticleCommentsRepliesResource;
use App\Models\CommentReply;

class ArticleCommentRepliesController extends Controller
{
    public function index(ArticleCommentRepliesRequest $request)
    {
        $data = CommentReply::with('user')
            ->where("comment_id", $request->comment_id)
            ->paginate(10);
        return $this->response(ArticleCommentsRepliesResource::collection($data));
    }
}
