<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoriesRequest;
use App\Http\Resources\CategoriesResource;
use App\Models\Category;

class CategoriesController extends Controller
{
    public function index(CategoriesRequest $request)
    {
        $data = Category::paginate(10);
        return $this->response(CategoriesResource::collection($data));
    }
}
