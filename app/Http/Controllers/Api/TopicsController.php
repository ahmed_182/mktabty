<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\TopicsRequest;
use App\Http\Resources\TopicsResource;
use App\Models\Topic;

class TopicsController extends Controller
{
    public function index(TopicsRequest $request)
    {
        $data = Topic::paginate(10);
        return $this->response(TopicsResource::collection($data));
    }
}
