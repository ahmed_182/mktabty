<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\BookDetailsRequest;
use App\Http\Resources\BookDetailsResource;
use App\Models\Book;

class BookDetailsController extends Controller
{
    public function index(BookDetailsRequest $request)
    {
        $data = Book::where("id", $request->book_id)
            ->with(["category", "sub_category"])
            ->first();
        $data->increment("no_views");
        return $this->response(BookDetailsResource::make($data));
    }
}
