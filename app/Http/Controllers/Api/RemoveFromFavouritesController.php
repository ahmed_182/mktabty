<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\RemoveFromFavouritesRequest;
use App\Models\Book;
use App\Models\Favorite;

class RemoveFromFavouritesController extends Controller
{
    public function index(RemoveFromFavouritesRequest $request)
    {
        $book = Book::find($request->bookId);
        $data = Favorite::where("bookId", $book->id)->where("userId", auth()->user()->id);
        $data->delete();
        return $this->response(null);
    }
}
