<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\DeleteArticleRequest;
use App\Models\Article;

class DeleteArticleController extends Controller
{
    public function index(DeleteArticleRequest $request)
    {
        // todo check user
        $data = Article::findOrFail($request->article_id);
        $data->delete();
        return $this->response(null);
    }
}
