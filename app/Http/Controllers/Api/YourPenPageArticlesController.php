<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\YourPenPageArticlesRequest;
use App\Http\Resources\ArticleResource;
use App\Models\Article;

class YourPenPageArticlesController extends Controller
{
    public function index(YourPenPageArticlesRequest $request)
    {
        $data = Article::with(["user", "comments"])
            ->paginate(10);
        return $this->response(ArticleResource::collection($data));
    }
}
