<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\SearchBooksRequest;
use App\Http\Resources\BooksResource;
use App\Models\Book;

class SearchBooksController extends Controller
{
    public function index(SearchBooksRequest $request)
    {
        $query = Book::select("id", "imgURL","bookName")->where("fileTypeId", 1);
        if ($request->categoryId)
            $query->where("categoryId", $request->categoryId);
        if ($request->subCategoryId)
            $query->where("subCategoryId", $request->subCategoryId);
        if ($request->key)
            $query->where("bookName", $request->key);
        $data = $query->paginate(10);
        return $this->response(BooksResource::collection($data));
    }
}

