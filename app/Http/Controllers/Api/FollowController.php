<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\FollowRequest;
use App\Models\Follow;

class FollowController extends Controller
{
    public function index(FollowRequest  $request)
    {
        $data = $request->validated();
        $data["userId"] = auth()->user()->id;
        Follow::create($data);
        return $this->response(null);
    }
}
