<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\AudioBookDetailsRequest;
use App\Http\Resources\AudioBookDetailsResource;
use App\Http\Resources\BookDetailsResource;
use App\Models\Book;
use Illuminate\Support\Facades\DB;

class AudioBookDetailsController extends Controller
{
    public function index(AudioBookDetailsRequest $request)
    {
        $data = Book::where("id", $request->book_id)
            ->with(["category", "sub_category"])
            ->first();
        return $this->response(AudioBookDetailsResource::make($data));
    }
}
