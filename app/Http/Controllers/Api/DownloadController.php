<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\DownloadRequest;
use App\Models\Download;

class DownloadController extends Controller
{
    public function index(DownloadRequest $request)
    {
        $data = $request->validated();
        $data["userId"] = auth()->user()->id;
        Download::create($data);
        return $this->response(null);
    }
}
