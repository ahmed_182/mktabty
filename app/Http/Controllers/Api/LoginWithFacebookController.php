<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Article;

class LoginWithFacebookController extends Controller
{
    public function index(LoginWithFacebookRequest  $request)
    {
        $data = $request->validated();
        $data["userId"] = auth()->user()->id;
        Article::create($data);
        return $this->response(null);
    }
}
