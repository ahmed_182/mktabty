<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdatePersonalInformationRequest;
use App\Http\Resources\ProfileResource;

class UpdatePersonalInformationController extends Controller
{
    public function index(UpdatePersonalInformationRequest $request)
    {
        $data = $request->validated();
        $user = auth()->user();
        $user->update($data);
        return $this->response(ProfileResource::make($user));
    }
}
