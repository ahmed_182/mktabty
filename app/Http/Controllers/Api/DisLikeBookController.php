<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\DisLikeBookRequest;
use App\Http\Requests\LikeBookRequest;
use App\Models\Book;
use App\Models\BookInteraction;

class DisLikeBookController extends Controller
{
    public function index(DisLikeBookRequest  $request)
    {
        $data = $request->validated();
        $data["user_id"] = auth()->user()->id;
        $data["type"] = BookInteraction::typeDisLike;
        BookInteraction::create($data);
        Book::findOrFail($data["book_id"])->increment("no_dislikes");
        return $this->response(null);
    }
}
