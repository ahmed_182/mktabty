<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\FollowersRequest;
use App\Http\Requests\FollowingsRequest;
use App\Http\Resources\MyFollowersResource;
use App\Models\Follow;
use Illuminate\Support\Facades\Auth;

class FollowersController extends Controller
{
    public function index(FollowersRequest $request)
    {

        $data = Follow::with(["follower"])
            ->where("followerId", auth()->user()->id)
            ->paginate(10);
        return $this->response(MyFollowersResource::collection($data));
    }


}
