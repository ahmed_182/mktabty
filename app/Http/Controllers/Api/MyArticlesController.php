<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\MyArticlesRequest;
use App\Http\Resources\ArticleResource;
use App\Models\Article;

class MyArticlesController extends Controller
{
    public function index(MyArticlesRequest $request)
    {
        $data = Article::where("userId", auth()->user()->id)
            ->with(["user"])->paginate(10);
        return $this->response(ArticleResource::collection($data));
    }
}
