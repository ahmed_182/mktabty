<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\IntroductionMessagesRequest;
use App\Http\Resources\IntroductionMessagesResource;
use App\Models\IntroductionMessage;

class IntroductionMessagesController extends Controller
{
    public function index(IntroductionMessagesRequest  $request)
    {
        $data = IntroductionMessage::paginate(10);
        return $this->response(IntroductionMessagesResource::collection($data));
    }
}
