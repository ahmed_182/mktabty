<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ShareArticleRequest;
use App\Models\Article;
use App\Models\ArticleInteraction;
use App\Models\BookInteraction;

class ShareArticleController extends Controller
{
    public function index(ShareArticleRequest $request)
    {
        $data = $request->validated();
        $data["user_id"] = auth()->user()->id;
        $data["type"] = BookInteraction::typeShare;
        ArticleInteraction::create($data);
        Article::findOrFail($data["article_id"])->increment("no_shares");
        return $this->response(null);
    }
}
