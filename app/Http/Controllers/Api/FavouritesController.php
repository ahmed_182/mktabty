<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\FavouritesRequest;
use App\Http\Resources\FavouritesResource;
use App\Models\Favorite;

class FavouritesController extends Controller
{
    public function index(FavouritesRequest $request)
    {
        $data = Favorite::with(["book"])
            ->where("userId", auth()->user()->id)
            ->paginate(10);
        return $this->response(FavouritesResource::collection($data));
    }
}
