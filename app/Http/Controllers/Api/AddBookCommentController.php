<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddBookCommentRequest;
use App\Models\Book;
use App\Models\BookComment;

class AddBookCommentController extends Controller
{
    public function index(AddBookCommentRequest $request)
    {
        $data = $request->validated();
        $data["user_id"] = auth()->user()->id;
        BookComment::create($data);
        // Book::findOrFail($data["book_id"])->increment("no_of_comments");
        return $this->response(null);
    }
}
