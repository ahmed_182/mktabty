<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Http\Resources\RegisterResource;
use App\Models\User;
use App\Models\UserTopic;
use App\Models\UserTopics;
use App\ModulesConst\UserTyps;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    public function index(RegisterRequest $request)
    {
        $data = $request->validated();
        $data["user_type_id"] = UserTyps::user;
        $data["api_token"] = Str::random(60);
        $user = User::create($data);
        $this->setUserCategories($user->id, $data["topics_ids"]);
        return $this->response(RegisterResource::make($user));
    }

    public function setUserCategories($user_id, $categories_ids)
    {
        $data["user_id"] = $user_id;
        foreach ($categories_ids as $id) {
            $data["topic_id"] = $id;
            UserTopic::create($data);
        }
    }
}
