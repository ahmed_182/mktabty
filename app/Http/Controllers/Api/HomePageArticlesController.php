<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\HomePageArticlesRequest;
use App\Http\Resources\ArticleResource;
use App\Models\Article;

class HomePageArticlesController extends Controller
{
    public function index(HomePageArticlesRequest $request)
    {
        $data = Article::with(["user", "comments"])
            ->paginate(10);
        return $this->response(ArticleResource::collection($data));
    }
}
