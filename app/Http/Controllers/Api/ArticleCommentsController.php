<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleCommentsRequest;
use App\Http\Resources\ArticleCommentsResource;
use App\Models\Comment;

class ArticleCommentsController extends Controller
{
    public function index(ArticleCommentsRequest $request)
    {
        $data = Comment::with('user')
            ->where("articleId", $request->articleId)
            ->paginate(10);
        return $this->response(ArticleCommentsResource::collection($data));
    }
}
