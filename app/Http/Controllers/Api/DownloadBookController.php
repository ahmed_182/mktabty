<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\DownloadBookRequest;
use App\Models\Download;

class DownloadBookController extends Controller
{
    public function index(DownloadBookRequest $request)
    {
        $data = $request->validated();
        $data["userId"] = auth()->user()->id;
        Download::create($data);
        return $this->response(null);
    }
}
