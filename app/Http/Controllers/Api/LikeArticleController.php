<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LikeArticleRequest;
use App\Models\Article;
use App\Models\ArticleInteraction;
use App\Models\BookInteraction;

class LikeArticleController extends Controller
{
    public function index(LikeArticleRequest  $request)
    {
        $data = $request->validated();
        $data["user_id"] = auth()->user()->id;
        $data["type"] = BookInteraction::typeLike;
        ArticleInteraction::create($data);
        Article::findOrFail($data["article_id"])->increment("no_likes");
        return $this->response(null);
    }
}
