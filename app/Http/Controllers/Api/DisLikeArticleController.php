<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\DisLikeArticleRequest;
use App\Models\Article;
use App\Models\ArticleInteraction;
use App\Models\BookInteraction;

class DisLikeArticleController extends Controller
{
    public function index(DisLikeArticleRequest $request)
    {
        $data = $request->validated();
        $data["user_id"] = auth()->user()->id;
        $data["type"] = BookInteraction::typeDisLike;
        ArticleInteraction::create($data);
        Article::findOrFail($data["article_id"])->increment("no_dislikes");
        return $this->response(null);
    }
}
