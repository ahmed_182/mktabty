<?php

    namespace App\Http\Controllers\Api\ResetPassword;

    use App\Http\Requests\UserUpdatePasswordRequest;
    use App\Mail\EmailVerification;
    use App\Mail\resetPassworMail;
    use App\ModulesConst\UserVerify;
    use App\Traits\apiResponse;
    use App\Models\User;
    use App\Models\User_lastpasswords;
    use Auth;
    use Carbon\Carbon;
    use Hash;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Mail;

    class IndexController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {

            $request->validate([
                'email' => 'required',
            ]);

            // check if this mobile existed ..
            $emailCheck = User::where('email', $request->email)->get();
            if (!$emailCheck->count() > 0) {
                return $this->apiResponse($request, trans('language.not_Existemail'), null, false, 400);
            }

            $user = User::where('email', $request->email)->first();
            //$user->need_reset_password = 1;
            $user->passCode = rand(111111, 999999);
            $user->save();
            \Illuminate\Support\Facades\Auth::login($user);
            // email Handeler
            $this->email_verify($user, trans('language.activation_msg'));
            return $this->response($request);
        }


        public function response(Request $request)
        {
            /*$item = auth()->user();*/
            return $this->sendResponse($request, trans('language.activation_msg'), $item, true, $accessToken);
        }


        public function reset_new_password(Request $request)
        {
            $request->validate([
                'password' => 'required',
            ]);
            $user = $request->user();
            if (!$user)
                abort(404);
            $user->password = \Hash::make($request->password);
            $user->need_reset_password = 0;
            $user->save();
            return $this->apiResponse($request, trans('language.message'), null, true);
        }


        public function updatePassword(UserUpdatePasswordRequest $request)
        {

            if (!$request->user()->mobile_verified)
                return $this->apiResponse($request, trans('language.mobile_verified_false'), null, false);
            if ($request->user()->password and !Hash::check($request->password, $request->user()->password))
                return $this->apiResponse($request, trans('language.password_wrong'), null, false);

            $user = $request->user();
            $user->password = Hash::make($request->new_password);
            $user->save();

            return $this->apiResponse($request, trans('language.message'), $request->user(), true);

        }

        public function confirm_code(Request $request)
        {

            $request->validate([
                'mobile' => 'required',
                'code' => 'required',
                'is_forget_password' => '',
            ]);

            $data = $request->all();
            $mob = $data['mobile'];

            // check if this mobile existed ..
            $mobileCheck = User::where('mobile', $mob)->get();
            if (!$mobileCheck->count() > 0) {
                return $this->apiResponse($request, trans('language.not_Existemobile'), null, false, 400);
            }

            $user = User::where('mobile', $mob)->first();

            if (!$request->is_forget_password) {
                if ($user->activation_code == $request->code) {
                    $user->user_status_id = UserVerify::yes;
                    $user->mobile_verified_at = 1;
                    $user->save();
                    $this->smsActivationCodeHandler($user, trans('language.mobile_verified'));
                    return $this->apiResponse($request, trans('language.mobile_verified'), $user, true, 200);
                } else {
                    return $this->apiResponse($request, trans('language.invalid_code'), null, false, 400);
                }
            }

            // check expired of this passCode
            if ($user->activation_code != $request->code) {
                return $this->apiResponse($request, trans('language.invalid_code'), null, false, 400);
            }
            $user->user_status_id = UserVerify::yes;
            $user->need_reset_password = 0;
            $user->save();
            Auth::login($user);
            $token = Auth::user()->createToken('myMob');
            $accessToken = $token->accessToken;
            $info['access_token'] = $accessToken;
            $this->smsActivationCodeHandler($user, trans('language.action_verified_mobile'));
            return $this->response($request);
        }

        public function resend_password_code(Request $request)
        {
            $request->validate([
                'is_forget_password' => '',
            ]);

            $user = $request->user();
            $user->activation_code = rand(111111, 999999);
            if ($request->is_forget_password) {
                $user->need_reset_password = 1;
            }
            $user->save();
            // sms Handeler
            $this->smsActivationCodeHandler($user, trans('language.mobile_verified_code'));
            return $this->apiResponse($request, trans('language.activation_msg'), null, true);
        }


        public function smsActivationCodeHandler($user, $message)
        {
            $mobile = $user->mobile;
            $message = $message;
            $this->sms($mobile, $message);
        }







    }
