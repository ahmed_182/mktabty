<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\RemoveFromDownloadsRequest;
use App\Models\Download;

class RemoveFromDownloadsController extends Controller
{
    public function index(RemoveFromDownloadsRequest $request)
    {
        $data = Download::findOrFail($request->download_id);
        $data->delete();
        return $this->response(null);
    }
}
