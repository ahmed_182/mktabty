<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\FollowingsRequest;
use App\Http\Resources\FollowingResource;
use App\Http\Resources\MyFollowersResource;
use App\Models\Follow;

class FollowingsController extends Controller
{
    public function index(FollowingsRequest $request)
    {
        $data = Follow::with(["follower"])
            ->where("userId", auth()->user()->id)
            ->paginate(10);
        return $this->response(FollowingResource::collection($data));
    }


}
