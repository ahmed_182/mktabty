<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UnFollowRequest;
use App\Models\Follow;

class UnFollowController extends Controller
{
    public function index(UnFollowRequest  $request)
    {
        $data = Follow::where("userId", auth()->user()->id)->where("followerId", $request->followerId)->firstOrFail();
        $data->delete();
        return $this->response(null);
    }
}
