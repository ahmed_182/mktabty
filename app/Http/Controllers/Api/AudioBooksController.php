<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\AudioBooksRequest;
use App\Http\Requests\BooksRequest;
use App\Http\Resources\AudioBooksResource;
use App\Models\Book;

class AudioBooksController extends Controller
{
    public function index(AudioBooksRequest $request)
    {
        $query = Book::select("id","imgURL","bookName")->where("fileTypeId", 2);
        if ($request->categoryId)
            $query->where("categoryId", $request->categoryId);
        if ($request->subCategoryId)
            $query->where("subCategoryId", $request->subCategoryId);
        $data = $query->paginate(10);
        return $this->response(AudioBooksResource::collection($data));
    }
}
