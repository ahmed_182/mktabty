<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleDetailsRequest;
use App\Http\Resources\ArticleResource;
use App\Models\Article;

class ArticleDetailsController extends Controller
{
    public function index(ArticleDetailsRequest $request)
    {
        $data = Article::where("id" , $request->article_id)
        ->with(["user"])->first();
        $data->increment("no_views");
        return $this->response(ArticleResource::make($data));
    }
}
