<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\DeleteBookCommentRequest;
use App\Models\BookComment;

class DeleteBookCommentController extends Controller
{
    public function index(DeleteBookCommentRequest $request)
    {
        // todo check user
        $data = BookComment::findOrFail($request->comment_id);
        $data->delete();
        return $this->response(null);
    }
}
