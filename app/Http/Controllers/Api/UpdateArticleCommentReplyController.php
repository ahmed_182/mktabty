<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateArticleCommentReplyRequest;
use App\Models\CommentReply;

class UpdateArticleCommentReplyController extends Controller
{
    public function index(UpdateArticleCommentReplyRequest $request)
    {
        // todo check user
        $data = $request->validated();
        CommentReply::findOrFail($request->comment_reply_id)->update($data);
        return $this->response(null);
    }
}
