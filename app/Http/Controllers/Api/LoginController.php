<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Resources\LoginResource;
use App\Models\UserTopic;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    public function index(LoginRequest $request)
    {
        $validateData["email"] = $request->email;
        $validateData["password"] = $request->password;
        $valid = auth()->guard('web')->attempt($validateData);
        if ($valid) {
            $user = auth()->user();
            $data = $request->validated();
            $data["api_token"] = Str::random(60);
            $user->update($data);
            $this->setUserCategories($user->id, $data["topics_ids"]);
            return $this->response(LoginResource::make($user));
        }
        return abort(401, "Unauthenticated");
    }

    public function setUserCategories($user_id, $categories_ids)
    {
        UserTopic::where("user_id", $user_id)->delete();
        $data["user_id"] = $user_id;
        foreach ($categories_ids as $id) {
            $data["topic_id"] = $id;
            UserTopic::create($data);
        }
    }
}
