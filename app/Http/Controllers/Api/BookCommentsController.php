<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\BookCommentsRequest;
use App\Http\Resources\BookCommentsResource;
use App\Models\BookComment;

class BookCommentsController extends Controller
{
    public function index(BookCommentsRequest $request)
    {
        $data = BookComment::where("book_id", $request->book_id)->paginate(10);
        return $this->response(BookCommentsResource::collection($data));
    }
}
