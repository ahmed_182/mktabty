<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserProfileRequest;
use App\Http\Resources\ProfileResource;
use App\Models\User;

class UserProfileController extends Controller
{
    public function index(UserProfileRequest $request)
    {
        return $this->response(ProfileResource::make(User::find($request->user_id)));
    }
}
