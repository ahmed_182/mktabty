<?php

namespace App\Http\Controllers;

trait FilesHandler
{
    private $dump = "https://via.placeholder.com/200x200.png/0077bb?text=et";

    public function imageHandler($databasePath)
    {
        return $databasePath ? $databasePath : $this->dump;
    }

    public function pdfHandler($databasePath)
    {
        return $databasePath ? $databasePath : $this->dump;
    }

    public function audioHandler($databasePath)
    {
        return $databasePath ? $databasePath : $this->dump;
    }

    public function storeFile($file)
    {
        $path = $file->store('public/images');
        $path = str_replace('public', '', $path);
        $serverPath = url('') . '/storage';
        $path = $serverPath . $path;
        return $path;
    }

}
