<?php

namespace App\Http\Controllers;

trait ResponseHandler
{
    public function response($data)
    {
        $response['success'] = true;
        if ($data)
            $response['data'] = $data;
        return $response;
    }
}
