<?php

namespace App\Http\Resources;

use App\Http\Controllers\FilesHandler;
use Illuminate\Http\Resources\Json\JsonResource;

class DownloadsResource extends JsonResource
{
    use FilesHandler;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data["id"] = $this->id;
        $data["imgURL"] = $this->book ? $this->imageHandler($this->book->imgURL) : "";
        $data["bookName"] = $this->book ? $this->book->bookName : "";
        return $data;
    }
}
