<?php

namespace App\Http\Resources;

use App\Http\Controllers\FilesHandler;
use Illuminate\Http\Resources\Json\JsonResource;

class ProfileResource extends JsonResource
{
    use FilesHandler;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data["id"] = $this->id;
        $data["image"] = $this->imageHandler($this->image);
        $data["name"] = $this->name;
        $data["bio"] = $this->bio;
        $data["no_articles"] =$this->myArticle;
        $data["my_followers"] = $this->myfollowers;
        $data["i_follow"] = $this->Isfollow;
        return $data;
    }
}
