<?php

namespace App\Http\Resources;

use App\Http\Controllers\FilesHandler;
use Illuminate\Http\Resources\Json\JsonResource;

class IntroductionMessagesResource extends JsonResource
{
    use FilesHandler;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data["image"] = $this->imageHandler($this->image);
        $data["title"] = $this->title;
        $data["text"] = $this->text;
        return $data;
    }
}
