<?php

namespace App\Http\Resources;

use App\Http\Controllers\FilesHandler;
use Illuminate\Http\Resources\Json\JsonResource;

class AudioBookDetailsResource extends JsonResource
{
    use FilesHandler;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data["id"] = $this->id;
        $data["imgURL"] = $this->imageHandler($this->imgURL);
        $data["bookURL"] = $this->pdfHandler($this->bookURL);
        $data["bookName"] = $this->bookName;
        $data["autherName"] = $this->autherName;
        $data["reader_name"] = $this->reader_name;
        $data["size"] = $this->book_size;
        $data["number_of_views"] = $this->no_views;
        $data["number_of_shares"] = $this->no_shares;
        $data["bookDescription"] = $this->bookDescription;
        // $data["average_rate"] = bcadd($this->average_rate , 0, 1); todo
        $data["average_rate"] = rand(5 ,1);
        $data["category"] = $this->categoryHandler($this->category, $this->sub_category);
        return $data;
    }

    public function categoryHandler($category, $sub_category)
    {
        $text = "";
        if ($category and $sub_category)
            $text .= $category->categoryName . " - " . $sub_category->subCategoryName;
        elseif ($category)
            $text .= $category->categoryName;
        elseif ($sub_category)
            $text .= $sub_category->subCategoryName;
        else
            $text = "has no category";
        return $text;
    }
}
