<?php

namespace App\Http\Resources;

use App\Http\Controllers\FilesHandler;
use Illuminate\Http\Resources\Json\JsonResource;

class MyFollowersResource extends JsonResource
{
    use FilesHandler;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data["user_id"] =  $this->user ? $this->user->id : 'invalid user';
        $data["name"] = $this->user ? $this->user->name : 'invalid user';
        $data["image"] = $this->imageHandler($this->image);
        return $data;
    }
}
