<?php

namespace App\Http\Resources;

use App\Http\Controllers\FilesHandler;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class BookDetailsCommentsResource extends JsonResource
{
    use FilesHandler;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data["id"] = $this->id;
        $data["user_name"] = $this->user ? $this->user->name : "";
        $data["user_id"] = $this->user ? $this->user->id : 'invalid user';
        $data["user_image"] = $this->user ? $this->imageHandler($this->user->image) : "";
        $data["created_at"] = (new Carbon($this->created_at))->diffForHumans();
        $data["title"] = $this->title;
        $data["body"] = $this->body;
        $data["no_likes"] = $this->no_likes;
        $data["no_dislikes"] = $this->no_dislikes;
        $data["no_comments"] = $this->no_comments;
        $data["no_interactions"] = $this->no_dislikes + $this->no_comments + $this->no_likes;
        $data["comments"] = [];
        return $data;
    }
}
