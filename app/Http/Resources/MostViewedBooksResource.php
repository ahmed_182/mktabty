<?php

namespace App\Http\Resources;

use App\Http\Controllers\FilesHandler;
use Illuminate\Http\Resources\Json\JsonResource;

class MostViewedBooksResource extends JsonResource
{
    use FilesHandler;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data["id"] = $this->id;
        $data["imgURL"] = $this->imageHandler($this->imgURL);
        $data["bookName"] = $this->bookName;
        $data["number_of_views"] = $this->number_of_views;
        return $data;
    }
}
