<?php

namespace App\Http\Resources;

use App\Http\Controllers\FilesHandler;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class MyTopicsResource extends JsonResource
{
    use FilesHandler;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data["id"] = $this->id;
        $data["name"] = $this->name ;
        $data["image"] = $this->image ? $this->imageHandler($this->image) : "";
        return $data;
    }
}
