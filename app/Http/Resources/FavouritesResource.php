<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FavouritesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data["id"] = $this->id;
        $data["bookId"] = $this->book ? $this->book->id : 'invalid user';
        $data["imgURL"] = $this->book->imageHandler($this->imgURL);
        $data["bookName"] = $this->book->bookName;
        return $data;
    }
}
