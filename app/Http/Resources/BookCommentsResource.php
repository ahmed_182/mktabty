<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class BookCommentsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data["id"] = $this->id;
        $data["user_name"] = $this->user ? $this->user->name : 'invalid user';
        $data["comment"] = $this->comment;
        $data["rate"] = $this->rate;
        $data["number_of_likes"] = 0;
        $data["number_of_dislikes"] = 0;
        $data["created_at"] = (new Carbon($this->created_at))->diffForHumans();
        return $data;
    }
}


