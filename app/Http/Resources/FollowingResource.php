<?php

namespace App\Http\Resources;

use App\Http\Controllers\FilesHandler;
use Illuminate\Http\Resources\Json\JsonResource;

class FollowingResource extends JsonResource
{
    use FilesHandler;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data["user_id"] =  $this->follower ? $this->follower->id : 'invalid user';
        $data["name"] = $this->follower ? $this->follower->name : 'invalid user';
        $data["image"] = $this->imageHandler($this->image);
        return $data;
    }
}
