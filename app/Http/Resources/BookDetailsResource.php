<?php

namespace App\Http\Resources;

use App\Http\Controllers\FilesHandler;
use App\Models\BookComment;
use App\Models\Favorite;
use Illuminate\Http\Resources\Json\JsonResource;

class BookDetailsResource extends JsonResource
{
    use FilesHandler;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data["id"] = $this->id;
        $data["imgURL"] = $this->imageHandler($this->imgURL);
        $data["bookURL"] = $this->pdfHandler($this->bookURL);
        $data["bookName"] = $this->bookName;
        $data["autherName"] = $this->autherName;
        $data["size"] = $this->book_size;
        $data["number_of_views"] = $this->no_views;
        $data["number_of_shares"] = $this->no_shares;
        $data["bookDescription"] = $this->bookDescription;
        $data["average_rate"] = $this->avg_rate();
        $data["category"] = $this->categoryHandler($this->category, $this->sub_category);
        $data["is_favourite"] = $this->is_favourite();
        return $data;
    }

    public function is_favourite()
    {
        // todo eager loading
        if (!auth()->user())
            return false;
        $flag = Favorite::where("bookId", $this->id)->where("userId", auth()->user()->id)->first();
        if ($flag)
            return true;
        return false;
    }

    public function avg_rate()
    {
        $avg = BookComment::where("book_id", $this->id)->avg('rate');
        return bcadd($avg, 0, 1);
    }

    public function categoryHandler($category, $sub_category)
    {
        $text = "";
        if ($category and $sub_category)
            $text .= $category->categoryName . " - " . $sub_category->subCategoryName;
        elseif ($category)
            $text .= $category->categoryName;
        elseif ($sub_category)
            $text .= $sub_category->subCategoryName;
        else
            $text = "has no category";
        return $text;
    }
}
