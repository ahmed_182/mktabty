<?php

namespace App\Http\Resources;

use App\Http\Controllers\FilesHandler;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ArticleCommentsResource extends JsonResource
{
    use FilesHandler;
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data["id"] = $this->id;
        $data["body"] = $this->body;
        $data["user_id"] = $this->user ? $this->user->id : 'invalid user';
        $data["user_image"] = $this->user ? $this->imageHandler($this->user->image) : "";
        $data["user_name"] = $this->user ? $this->user->name : 'invalid user';
        $data["number_of_likes"] = 0;
        $data["number_of_dislikes"] = 0;
        $data["created_at"] = (new Carbon($this->created_at))->diffForHumans();
        return $data;
    }
}
