<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Lang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->header('lang') && in_array($request->header('lang'),["ar" , "en"]))
            app()->setLocale($request->header('lang'));
        else
            app()->setLocale("ar");
        return $next($request);
    }
}
