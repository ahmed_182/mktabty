<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FollowingsRequest extends FormRequest
{
    public function rules()
    {
        return [
            'page' => 'required|numeric',
            'user_id' => 'required',
        ];
    }
}
