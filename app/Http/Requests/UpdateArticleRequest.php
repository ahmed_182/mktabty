<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateArticleRequest extends FormRequest
{
    public function rules()
    {
        return [
            'article_id' => 'required|exists:articles,id',
            'audio_url' => 'string',
            'book_id' => 'numeric',
            'image' => 'file',
            'title' => 'required|string',
            'body' => 'required|string',
        ];
    }
}
