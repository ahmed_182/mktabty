<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleCommentsRequest extends FormRequest
{
    public function rules()
    {
        return [
            'page' => 'required|numeric',
            'articleId' => 'required|exists:articles,id',
        ];
    }
}
