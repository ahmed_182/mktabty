<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RemoveFromFavouritesRequest extends FormRequest
{
    public function rules()
    {
        return [
            'bookId' => 'required|exists:books,id,fileTypeId,1',
        ];
    }
}
