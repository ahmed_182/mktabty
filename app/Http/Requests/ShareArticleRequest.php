<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ShareArticleRequest extends FormRequest
{
    public function rules()
    {
        return [
            'article_id' => 'required|exists:articles,id',
        ];
    }
}
