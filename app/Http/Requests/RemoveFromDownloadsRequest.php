<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RemoveFromDownloadsRequest extends FormRequest
{
    public function rules()
    {
        return [
            'download_id' => 'required|exists:downloads,id',
        ];
    }
}
