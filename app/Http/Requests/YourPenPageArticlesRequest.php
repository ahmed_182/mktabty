<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class YourPenPageArticlesRequest extends FormRequest
{
    public function rules()
    {
        return [
            'page' => 'required|numeric',
            'lang' => 'in:ar,en',
        ];
    }
}
