<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    public function rules()
    {
        return [
            'gender' => "required|in:1,2",
            'deviceType' => "required|string",
            'fireBaseToken' => "required",
            'birth_date' => 'required|date',
            'topics_ids' => "required|array",
            'name' => 'required|string',
            'email' => 'required|unique:users,email',
            'password' => 'required',
        ];
    }
}
