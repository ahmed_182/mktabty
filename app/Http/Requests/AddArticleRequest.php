<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddArticleRequest extends FormRequest
{
    public function rules()
    {
        return [
            'book_id' => 'numeric',
            'audio_url' => 'string',
            'title' => 'required|string',
            'body' => 'required|string',
            'image' => 'required|file',
        ];
    }
}
