<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DeleteArticleCommentRequest extends FormRequest
{
    public function rules()
    {
        return [
            'comment_id' => 'required|exists:comments,id',
        ];
    }
}
