<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DeleteArticleCommentReplyRequest extends FormRequest
{
    public function rules()
    {
        return [
            'comment_reply_id' => 'required|exists:comment_replies,id',
        ];
    }
}
