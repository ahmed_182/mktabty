<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DownloadsRequest extends FormRequest
{
    public function rules()
    {
        return [
            'page' => 'required|numeric',
        ];
    }
}
