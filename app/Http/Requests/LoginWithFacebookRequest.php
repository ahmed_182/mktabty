<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginWithFacebookRequest extends FormRequest
{
    public function rules()
    {
        return [
            'fireBaseToken' => 'required|string',
            'social_id' => 'required|string|unique:users',
            'loginBy_id' => 'required|string',
        ];
    }
}
