<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookCommentsRequest extends FormRequest
{
    public function rules()
    {
        return [
            'page' => 'required|numeric',
            'book_id' => 'required|exists:books,id',
        ];
    }
}
