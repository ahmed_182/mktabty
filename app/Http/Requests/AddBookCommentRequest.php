<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddBookCommentRequest extends FormRequest
{
    public function rules()
    {
        return [
            'book_id' => 'required|exists:books,id',
            'comment' => 'required|string',
            'rate' => 'required|numeric',
        ];
    }
}
