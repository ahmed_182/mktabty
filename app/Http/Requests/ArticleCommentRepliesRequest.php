<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleCommentRepliesRequest extends FormRequest
{
    public function rules()
    {
        return [
            'page' => 'required|numeric',
            'comment_id' => 'required|exists:comments,id',
        ];
    }
}
