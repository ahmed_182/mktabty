<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UnFollowRequest extends FormRequest
{
    public function rules()
    {
        return [
            'followerId' => 'required|numeric|exists:users,id',
        ];
    }
}
