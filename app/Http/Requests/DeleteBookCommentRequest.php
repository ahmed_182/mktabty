<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DeleteBookCommentRequest extends FormRequest
{
    public function rules()
    {
        return [
            'comment_id' => 'required|exists:books,id',
        ];
    }
}
