<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DownloadBookRequest extends FormRequest
{
    public function rules()
    {
        return [
            'bookId' => 'required|exists:books,id',
        ];
    }
}
