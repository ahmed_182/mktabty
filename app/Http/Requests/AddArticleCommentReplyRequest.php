<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddArticleCommentReplyRequest extends FormRequest
{
    public function rules()
    {
        return [
            'text' => 'required|string',
            'comment_id' => 'required|exists:comments,id',
        ];
    }
}
