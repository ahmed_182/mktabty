<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestBookRequest extends FormRequest
{
    public function rules()
    {
        return [
            'page' => 'required|numeric',
            'lang' => 'in:ar,en',
            'userName' => 'required|string',
            'phone' => 'required|string',
            'bookName' => 'required|string',
            'author' => 'required|string',
        ];
    }
}
