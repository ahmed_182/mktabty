<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBookCommentRequest extends FormRequest
{
    public function rules()
    {
        return [
            'comment' => 'required|string',
        ];
    }
}
