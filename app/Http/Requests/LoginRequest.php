<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    public function rules()
    {
        return [
            'gender' => "required|in:1,2",
            'deviceType' => "required|string",
            'fireBaseToken' => "required",
            'birth_date' => 'required|date',
            'topics_ids' => "required|array",
            'email' => 'required|exists:users,email',
            'password' => 'required',
        ];
    }
}
