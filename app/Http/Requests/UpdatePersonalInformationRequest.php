<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePersonalInformationRequest extends FormRequest
{
    public function rules()
    {
        return [
            'image' => 'file',
            'name' => 'string',
            'email' => 'email|unique:users,email',
            'phone' => 'string',
            'bio' => 'string',
        ];
    }
}
