<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddArticleCommentRequest extends FormRequest
{
    public function rules()
    {
        return [
            'body' => 'required|string',
            'articleId' => 'required|exists:articles,id',
        ];
    }
}
