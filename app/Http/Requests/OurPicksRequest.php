<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OurPicksRequest extends FormRequest
{
    public function rules()
    {
        return [
            'page' => 'required|numeric',
        ];
    }
}
