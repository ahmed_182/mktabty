<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleDetailsRequest extends FormRequest
{
    public function rules()
    {
        return [
            'article_id' => 'required|exists:articles,id',
        ];
    }
}
