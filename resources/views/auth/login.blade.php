<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{trans('language.appName')}}</title>

    <link rel="stylesheet" href="{{url('assets/admin/vendors/core/core.css')}}">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- end plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{url('assets/admin/fonts/feather-font/css/iconfont.css')}}">
    <link rel="stylesheet" href="{{url('assets/admin/vendors/flag-icon-css/css/flag-icon.min.css')}}">
    <!-- endinject -->
@if (app()->getLocale() == "ar")
    <!-- Call Arabic Css File -->
        <link href="https://fonts.googleapis.com/css2?family=Cairo&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="{{url('assets/admin/css/ar_css.css')}}">
@endif
<!-- Layout styles -->
    <link rel="stylesheet" href="{{url('assets/admin/css/demo_1/style.css')}}">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="{{url('assets/admin/images/logo.png')}}"/>
</head>
@if(app()->getLocale() == 'ar')
    <body class="rtl">
    @else
        <body>

        @endif
        <div class="main-wrapper">
            <div class="page-wrapper full-page">
                <div class="page-content d-flex align-items-center justify-content-center">

                    <div class="row w-100 mx-0 auth-page">
                        <div class="col-md-8 col-xl-6 mx-auto">
                            <div class="card">
                                <div class="row">
                                    <div class="col-md-4 pl-md-0">
                                        <div class="auth-left-wrapper"
                                             style="background-image:url({{url('assets/admin/images/logo.png')}}); height: 452px !important; width: 219px !important; ">

                                        </div>
                                    </div>
                                    <div class="col-md-8 pr-md-0">
                                        <div class="auth-form-wrapper px-4 py-5">
                                            <a href="#"
                                               class="noble-ui-logo d-block mb-2">{{trans('language.appName')}}</a>
                                            <form class="forms-sample" id="loginform" method="POST"
                                                  action="{{ url('adminlogin') }}">
                                                @csrf
                                                <div class="form-group m-t-40">
                                                    @if ($errors->any())
                                                        <div class="alert alert-danger">
                                                            <ul>
                                                                @foreach ($errors->all() as $error)
                                                                    <li>{{ $error }}</li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    @endif

                                                    @if(Session::has('danger'))
                                                        <div class="alert alert-danger"
                                                             style="font-weight: bold"> {{ Session::get('danger') }}</div>
                                                    @endif
                                                    <div class="form-group">
                                                        <label
                                                            for="exampleInputEmail1">{{ __('language.email') }}</label>
                                                        <input type="email" name="email" class="form-control"
                                                               id="exampleInputEmail1"
                                                               placeholder="{{ __('language.email') }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label
                                                            for="exampleInputPassword1">{{ __('language.password') }}</label>
                                                        <input type="password" name="password" class="form-control"
                                                               id="exampleInputPassword1"
                                                               autocomplete="current-password"
                                                               placeholder="{{ __('language.password') }}">
                                                    </div>

                                                    <div class="mt-3">
                                                        <button type="submit"
                                                                class="btn btn-primary text-white ml-2 mb-2 mb-md-0"
                                                                style="background-color: #705BDE!important;  border: black;">{{ __('language.login') }}</button>
                                                        @if(app()->getLocale() == 'en')
                                                            <a href="{{ url("".url()->full()."?&lang=ar") }}"
                                                               class="btn btn-outline-primary btn-icon-text mb-2 mb-md-0"
                                                               style="background-color: #031A6F!important;border: black; color: #fff;
">
                                                                <i class="btn-icon-prepend"></i>
                                                                {{trans('اللغه العربيه')}}  </a>
                                                        @else
                                                            <a href="{{ url("".url()->full()."?&lang=en") }}"
                                                               class="btn btn-outline-primary btn-icon-text mb-2 mb-md-0"
                                                               style="background-color: #031A61!important; border: black; color: #fff;
">
                                                                <i class="btn-icon-prepend"></i>
                                                                {{trans('English')}}
                                                            </a>
                                                        @endif

                                                    </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        {{--
        <script src="{{url('assets/admin/js/jquery.min.js')}}../"></script>
        <script src="{{url('assets/admin/js/template.js')}}../"></script>
        --}}
        <!-- core:js -->
        <script src="{{url('assets/admin/vendors/core/core.js')}}"></script>
        <!-- endinject -->
        <!-- plugin js for this page -->
        <!-- end plugin js for this page -->
        <!-- inject:js -->
        <script src="{{url('assets/admin/vendors/feather-icons/feather.min.js')}}"></script>
        <script src="assets/admin/js/template.js"></script>
        <!-- endinject -->
        <!-- custom js for this page -->
        <!-- end custom js for this page -->
        </body>
</html>
