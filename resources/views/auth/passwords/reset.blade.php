<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->


<div class="container">
    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- Start form -->
            <form action="{{url("/change-password")}}"
                  enctype="multipart/form-data" method="post">
                @csrf

                <input type="hidden" value="{{$item->api_token}}" name="user_token">

                <div class="form-group">
                    <label for="exampleInputEmail1">new password</label>
                    <input type="password" name="password" class="form-control" id="exampleInputEmail1"
                           aria-describedby="emailHelp" placeholder="password">
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">password confirmation</label>
                    <input type="password" name="password_confirmation" class="form-control" id="exampleInputEmail1"
                           aria-describedby="emailHelp" placeholder="password confirmation">
                </div>

                <div class="form-check">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>

            </form>


            <!-- End form -->
        </div>


    </div>
</div>

<script>


    jQuery(document).ready(function () {


        // Show password Button
        $("#showpassword").on('click', function () {

            var pass = $("#password");
            var fieldtype = pass.attr('type');
            if (fieldtype == 'password') {
                pass.attr('type', 'text');
                $(this).text("Hide Password");
            } else {
                pass.attr('type', 'password');
                $(this).text("Show Password");
            }


        });


    });


</script>
