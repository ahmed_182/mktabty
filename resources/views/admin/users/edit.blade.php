@extends('admin.layout.forms.edit.index')
@section('action' , "users/$item->id")
@section('root' , "users")
@section('title' , trans('language.edit'))
@section('page-title',trans('language.edit'))
@section('form-groups')
    @includeIf('admin.components.form.edit.text', ['label' => trans('language.name'),'name'=>'name', 'placeholder'=>trans('language.name'),'valid'=>trans('language.vaildation')])
    @includeIf('admin.components.form.edit.email', ['label' => trans('language.email'),'name'=>'email', 'placeholder'=>trans('language.email')])
    @includeIf('admin.components.form.edit.text', ['label' => trans('language.mobile'),'name'=>'phone', 'placeholder'=>trans('language.mobile'),'valid'=>trans('language.vaildation')])
    @includeIf('admin.components.form.edit.textarea', ['label' => trans('language.bio'),'name'=>'bio', 'placeholder'=>trans('language.bio'),'valid'=>trans('language.vaildation')])
    @includeIf('admin.components.form.edit.password', ['icon' => 'fa fa-key','label' => trans('language.password'),'name'=>'password', 'placeholder'=>trans('language.password')])


@endsection
@section('submit-button-title' , trans('web.edit'))
