@extends('admin.layout.table.index')
@section('page-title',trans('language.topics'))
@section('buttons')

@stop
@section('nav')

    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.topics')}}</li>
    </ol>
@endsection

@section('thead')
    <th>#</th>
    <th>{{trans('language.image')}}</th>
    <th>{{trans('language.name')}}</th>
{{--
    <th>{{trans('language.settings')}}</th>
--}}
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td> @includeIf("admin.components.image.index" , ["url" => $item->dash_image])</td>
            <td>{{ $item->dash_topic }}</td>
           {{-- <td>
                @includeIf("admin.components.buttons.delete",["message" =>  "($item->dash_name)" ,  "action" => url("admin/topics/$item->id")])
            </td>--}}
        </tr>
    @endforeach
@endsection


