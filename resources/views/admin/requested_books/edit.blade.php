
@extends('admin.layout.forms.edit.index')
@section('action' , "requested-books/$item->id")
@section('title' , trans('language.edit'))
@section('page-title',trans('language.countries'))
@section('form-groups')
    <label>{{trans('language.status')}}</label>
    <div class="input-group">
        <select required class="form-control" name="status">
            <option value="0">{{trans('language.status')}}</option>

            <option value="1">الكتاب غير متوفر</option>
            <option value="2">تم اضافة الكتاب </option>
            <option value="3"> جديد </option>

        </select>
    </div>
<br>
@endsection
@section('submit-button-title' , trans('language.edit'))
