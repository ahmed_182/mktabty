@extends('admin.layout.table.index')
@section('page-title',trans('language.contact_us'))
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.requested_books')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.client')}}</th>
    <th>{{trans('language.mobile')}}</th>
    <th>{{trans('language.book_name')}}</th>
    <th>{{trans('language.auther')}}</th>
    <th>{{trans('language.status')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->userName}}</td>
            <td>{{$item->phone}}</td>
            <td>{{$item->bookName}}</td>
            <td>{{$item->author}}</td>
            <td>{{$item->dash_StatusName}}</td>
{{--
            <td>{{$item->created_at}}</td>
--}}
            <td>
                @includeIf("admin.components.buttons.custom" , ["href" => "requested-books/$item->id/edit", 'class' => 'btn btn-warning' , 'title'=> trans('language.status'), 'feather' => 'plus-circle'])
                @includeIf("admin.components.buttons.delete",["message" => ($item->name) ,  "action" => url("admin/requested_books/$item->id")])
            </td>
        </tr>
    @endforeach
@endsection

@section("filters")
    <form method="get" action="{{url("/admin/requested-books/")}}">

        <div style="display: flex">
            <div class="col-md-3">
                <input type="text" class="form-control name_input " name="userName" value="{{request()->userName}}"
                       placeholder="{{trans('language.name')}}">
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control email_input" name="phone" value="{{request()->phone}}"
                       placeholder="{{trans('language.mobile')}}">
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control  mobile_input " name="bookName" value="{{request()->bookName}}"
                       placeholder="{{trans('language.book_name')}}">
            </div>
            <div class="col-md-3">
                <input style="width: 45%" type="submit" class="btn btn-success " value="{{trans('language.filter')}}">
                <button style="width: 45%" type="button"
                        class="btn btn-info  reset_inputs ">{{trans('language.reset')}}</button>
            </div>
        </div>
    </form>
@stop

@section("extra_js")

    <script>
        $('.reset_inputs').click(function () {
            $('.name_input').val('');
            $('.email_input').val('');
            $('.mobile_input').val('');
        });
    </script>

@endsection



