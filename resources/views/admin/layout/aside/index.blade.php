<nav class="sidebar">
    <div class="sidebar-header">
        <a href="{{url("admin/")}}" class="sidebar-brand">
            {{trans('language.appName')}}
        </a>
        <div class="sidebar-toggler not-active">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
    <div class="sidebar-body">
        <ul class="nav">

            <li class="nav-item">

                @includeIf("admin.layout.aside.main-item" ,["href" => "#users_section" , "icon"=>"user", "title" => trans('language.users') , "description" => trans('web.list'), "feather"=>"user"])

                <div class="collapse" id="users_section">
                    <ul class="nav sub-menu">
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "users" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "users/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])
                    </ul>
                </div>
            </li>
            <br>


            <li class="nav-item">

                @includeIf("admin.layout.aside.main-item" ,["href" => "#categories" , "icon"=>"user", "title" => trans('language.categories') , "description" => trans('web.list'), "feather"=>"grid"])

                <div class="collapse" id="categories">
                    <ul class="nav sub-menu">
                         @includeIf("admin.layout.aside.sub-item" ,["href" => "categories" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "categories/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])
                    </ul>
                </div>
            </li>
            <br>

            <li class="nav-item">
                @includeIf("admin.layout.aside.main-item" ,["href" => "#openScreens" , "icon"=>"user", "title" => trans('language.openScreens') , "description" => trans('web.list'), "feather"=>"list"])
                <div class="collapse" id="openScreens">
                    <ul class="nav sub-menu">
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "openScreens" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "openScreens/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])
                    </ul>
                </div>
            </li>


            <br>

            <li class="nav-item">

                @includeIf("admin.layout.aside.main-item" ,["href" => "#books" , "icon"=>"user", "title" => trans('language.books') , "description" => trans('web.list'), "feather"=>"grid"])

                <div class="collapse" id="books">
                    <ul class="nav sub-menu">
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "books" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "books/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])
                    </ul>
                </div>
            </li>
            <br>

            <li class="nav-item">

                @includeIf("admin.layout.aside.main-item" ,["href" => "#audio_books" , "icon"=>"user", "title" => trans('language.audio_books') , "description" => trans('web.list'), "feather"=>"grid"])

                <div class="collapse" id="audio_books">
                    <ul class="nav sub-menu">
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "audio_books" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "audio_books/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])
                    </ul>
                </div>
            </li>


            <br>

            <li class="nav-item">

                @includeIf("admin.layout.aside.main-item" ,["href" => "#articles" , "icon"=>"user", "title" => trans('language.articles') , "description" => trans('web.list'), "feather"=>"list"])

                <div class="collapse" id="articles">
                    <ul class="nav sub-menu">
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "articles" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "articles/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])
                    </ul>
                </div>
            </li>


            <br>

            <li class="nav-item">

                @includeIf("admin.layout.aside.main-item" ,["href" => "#topics" , "icon"=>"user", "title" => trans('language.topics') , "description" => trans('web.list'), "feather"=>"grid"])

                <div class="collapse" id="topics">
                    <ul class="nav sub-menu">
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "topics" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "topics/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])
                    </ul>
                </div>
            </li>


            <br>
            <li class="nav-item">
                @includeIf("admin.layout.aside.main-item" ,["href" => "#notifications" , "icon"=>"user", "title" => trans('language.notifications') , "description" => trans('web.list'), "feather"=>"list"])
                <div class="collapse" id="notifications">
                    <ul class="nav sub-menu">
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "notifications" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "notifications/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])
                    </ul>
                </div>
            </li>
           <br>
            <li class="nav-item">
                @includeIf("admin.layout.aside.main-item" ,["href" => "#downloads" , "icon"=>"user", "title" => trans('language.downloads') , "description" => trans('web.list'), "feather"=>"list"])
                <div class="collapse" id="downloads">
                    <ul class="nav sub-menu">
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "downloads" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                    </ul>
                </div>
            </li>

            <br>
            <li class="nav-item">
                @includeIf("admin.layout.aside.main-item" ,["href" => "#requested_books" , "icon"=>"user", "title" => trans('language.requested_books') , "description" => trans('web.list'), "feather"=>"list"])
                <div class="collapse" id="requested_books">
                    <ul class="nav sub-menu">
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "requested-books" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                    </ul>
                </div>
            </li>


          {{--  <br>
            <li class="nav-item">

                @includeIf("admin.layout.aside.main-item" ,["href" => "#setting_section" , "icon"=>"user", "title" => trans('language.settings') , "description" => trans('web.list'), "feather"=>"settings"])

                <div class="collapse" id="setting_section">
                    <ul class="nav sub-menu">
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "settings" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                    </ul>
                </div>
            </li>--}}


        </ul>
    </div>
</nav>

