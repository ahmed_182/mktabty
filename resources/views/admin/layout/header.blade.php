<nav class="navbar">
    <a href="#" class="sidebar-toggler">
        <i data-feather="menu"></i>
    </a>
    <div class="navbar-content">
        <ul class="navbar-nav">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="languageDropdown" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    @if(app()->getLocale() == 'en')

                        <i class="flag-icon flag-icon-us mt-1" title="us"></i> <span
                            class="font-weight-medium ml-1 mr-1">{{trans('English')}} </span>
                    @else
                        <i class="flag-icon flag-icon-eg mt-1" title="qa"></i> <span
                            class="font-weight-medium ml-1 mr-1">{{trans('اللغه العربيه')}} </span>

                    @endif
                </a>
                <div class="dropdown-menu" aria-labelledby="languageDropdown">
                    @if(app()->getLocale() == 'en')
                        <a href="{{ url("".url()->full()."?&lang=ar") }}" class="dropdown-item py-2"><i
                                class="flag-icon flag-icon-eg" title="qa" id="eg"></i> <span
                                class="ml-1">  {{trans('اللغه العربيه')}}  </span></a>
                    @else
                        <a href="{{ url("".url()->full()."?&lang=en") }}" class="dropdown-item py-2"><i
                                class="flag-icon flag-icon-us" title="us" id="us"></i> <span
                                class="ml-1"> {{trans('English')}} </span></a>
                    @endif

                </div>
            </li>


            <li class="nav-item dropdown nav-profile">
                <a class="nav-link dropdown-toggle" href="#" id="profileDropdown" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    <p class="name font-weight-bold mb-0">{{auth()->User()->name}}</p>
                </a>
                <div class="dropdown-menu" aria-labelledby="profileDropdown">
                    <div class="dropdown-header d-flex flex-column align-items-center">
                        <div class="figure mb-3">
{{--
                            <img src="https://via.placeholder.com/80x80" alt="">
--}}
                        </div>
                        <div class="info text-center">
                            <p class="name font-weight-bold mb-0">{{auth()->User()->name}}</p>
                            <p class="email text-muted mb-3">{{auth()->User()->email}}</p>
                        </div>
                    </div>
                    <div class="dropdown-body">
                        <ul class="profile-nav p-0 pt-3">

                            <li class="nav-item">
                                <a href="javascript:;"
                                   onclick="event.preventDefault();document.getElementById('logout-form').submit();"
                                   class="nav-link">
                                    <i data-feather="log-out"></i>
                                    <span>{{trans('web.signOut')}}</span>
                                </a>
                                <form id="logout-form" action="{{ url('/adminlogout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</nav>
