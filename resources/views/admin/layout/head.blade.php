<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{trans('language.appName')}}</title>
    <link rel="stylesheet" href="{{url('assets/admin/vendors/core/core.css')}}">

    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="{{url('assets/admin/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css')}}">
    <!-- end plugin css for this page -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="{{url('assets/admin/vendors/datatables.net-bs4/dataTables.bootstrap4.css')}}">

    <!-- end plugin css for this page -->
    <!-- end plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{url('assets/admin/fonts/feather-font/css/iconfont.css')}}">
    <link rel="stylesheet" href="{{url('assets/admin/vendors/flag-icon-css/css/flag-icon.min.css')}}">
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="{{url('assets/admin/css/demo_1/style.css')}}">
    <link rel="stylesheet" href="{{url('assets/admin/css/dropify.min.css')}}">

    <!-- End layout styles -->
    <link rel="shortcut icon" href="{{url('assets/admin/images/logo.png')}}"/>

@if (app()->getLocale() == "ar")
    <!-- Call Arabic Css File -->
        <link href="https://fonts.googleapis.com/css2?family=Cairo&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="{{url('assets/admin/css/ar_css.css')}}">
    @endif


    <style>
        .searchBtn {
            margin-top: 24px;
            height: 47px;
            width: 139px;
        }

        .carousel-control-next-icon {
            background-image: url({{asset('assets/admin/images/forward.png')}});
        }

        .carousel-control-prev-icon {
            background-image: url("{{asset('assets/admin/images/back.png')}}");
        }

        .descCut {
            display: block;
            word-wrap: break-word;
            white-space: nowrap;
            width: 300px;
            overflow: hidden;
            text-overflow: ellipsis;
        }

        .c {
            display: block;
            width: 20px;
            height: 20px;
            border-radius: 100%;
            margin: 10px;
        }
    </style>


    @yield("css")
    {{-- <!--[if lt IE 9]>
         <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
         <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
         <![endif]-->

         <style>
             .border_bar {
                 border-bottom: 1px solid #404854;
             }
         </style>--}}

    @yield('extra_css')
</head>
