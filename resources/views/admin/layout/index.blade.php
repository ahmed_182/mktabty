<!DOCTYPE html>
<html lang="en">

@includeIf("admin.layout.head")

@if(app()->getLocale() == 'ar')
    <body class="rtl">
    @else
        <body>
        @endif
        <div class="main-wrapper">

            @includeIf("admin.layout.aside.index")

            <div class="page-wrapper">
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">

                        <a href="{{url("admin/")}}/@yield("root")"><h3 class="text-themecolor">@yield("page-title")</h3>
                        </a>
                    </div>
                </div>

                <!-- partial:partials/_navbar.html -->
            @includeIf("admin.layout.header")
            <!-- partial -->
                <div class="page-content">
                    <div class="container-fluid">

                        @if (\Session::has('danger'))
                            <div class="alert alert-danger top-alert alert-icon-left mb-0 fade-message" role="alert">
                                <span style="display: inline" class="alert-icon"><i class="fa fa-ban"></i></span>
                                <p style="display: inline" class="mb-0">{{ \Session::get('danger') }}</p>
                            </div>
                        @endif

                        @if (\Session::has('error'))
                            <div class="alert alert-danger top-alert alert-icon-left mb-0 fade-message" role="alert">
                                <span style="display: inline" class="alert-icon"><i class="fa fa-ban"></i></span>
                                <p style="display: inline" class="mb-0">{{ \Session::get('error') }}</p>
                            </div>
                        @endif

                        @if (\Session::has('success'))
                            <div class="alert alert-success top-alert alert-icon-left mb-0 fade-message" role="alert">
                                <span style="display: inline" class="alert-icon"><i class="fa fa-check"></i></span>
                                <p style="display: inline" class="mb-0">{{ \Session::get('success') }}</p>
                            </div>
                        @endif
                    </div>

                    @yield("content")
                </div>


                <footer class="footer d-flex flex-column flex-md-row align-items-center justify-content-between">
                    <p class="text-muted text-center text-md-left">Copyright © 2020 <a href="#" target="_blank">Vision
                            Apps</a>. All rights reserved</p>
                </footer>


            </div>


        </div>


        @includeIf("admin.components.modals.delete-modal")
        @includeIf("admin.components.modals.show-modal")

        @includeIf("admin.layout.scripts")
        </body>
</html>
