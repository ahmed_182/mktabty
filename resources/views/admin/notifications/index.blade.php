@extends('admin.layout.table.index')
@section('page-title',trans('language.notifications'))
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.appName')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.notifications')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>

    <th>{{trans('language.title')}}</th>
    <th>{{trans('language.body')}}</th>
    <th>{{trans('language.created_at')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->title}}</td>
            <td>{{$item->body}}</td>
            <td>{{$item->dash_created}}</td>
            <td>
                @includeIf("admin.components.buttons.delete",["message" =>  "($item->message)" ,  "action" => url("admin/contacts/$item->id")])
            </td>
        </tr>
    @endforeach
@endsection


