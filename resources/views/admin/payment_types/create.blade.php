@extends('admin.layout.forms.add.index')
@section('action' , "payment_types")
@section('title' , trans('language.add'))
@section('page-title',trans('language.payment_types'))
@section('form-groups')


    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.name_ar'),'name'=>'name_ar', 'placeholder'=>trans('language.name_ar'),'valid'=>trans('language.vaildation')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.name_en'),'name'=>'name_en', 'placeholder'=>trans('language.name_en'),'valid'=>trans('language.vaildation')])


@endsection
@section('submit-button-title' , trans('language.add'))
