@extends('admin.layout.table.index')
@section('page-title',trans('language.categories'))
@section('buttons')

@stop
@section('thead')
    <th>#</th>
    <th>{{trans('language.image')}}</th>
    <th>{{trans('language.category')}}</th>
    <th>{{trans('language.addsubcategory')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td> @includeIf("admin.components.image.index" , ["url" => $item->dash_image])</td>
            <td>{{$item->dash_name}}</td>
            <td>@includeIf("admin.components.buttons.addbtn" , ["href" => "categories/$item->id/subcategory",'class' => 'info' , 'title'=> trans('language.addsubcategory') ])</td>
            <td>
                @includeIf("admin.components.buttons.edit" , ["href" => "categories/$item->id/edit"])

                @includeIf("admin.components.buttons.delete",["message" => "" ,  "action" => url("admin/categories/$item->id")])

            </td>
        </tr>
    @endforeach
@endsection


