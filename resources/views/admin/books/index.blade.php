@extends('admin.layout.table.index')
@section('page-title',trans('language.categories'))
@section('buttons')

@stop
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.books')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.name')}}</th>
    <th>{{trans('language.category')}}</th>
    <th>{{trans('language.sub_category')}}</th>
    <th>{{trans('language.image')}}</th>
    <th>{{trans('language.comments')}}</th>
    <th>{{trans('language.rates')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->bookName}}</td>
            <td>{{$item->Dashcategory}}</td>
            <td>{{$item->DashSubCategory}}</td>
            <td> @includeIf("admin.components.image.index" , ["url" => $item->imgURL])</td>
            <td>

                @includeIf("admin.components.buttons.custom" , ["href" => "books/$item->id/comments", 'class' => 'bi bi-bell' , 'title'=> trans('language.comments'), 'feather' => 'list'])

            </td>

            <td>

                @includeIf("admin.components.buttons.custom" , ["href" => "books/$item->id/rates", 'class' => 'bi bi-bell' , 'title'=> trans('language.rates'), 'feather' => 'list'])

            </td>
            <td>
                @includeIf("admin.components.buttons.edit" , ["href" => "books/$item->id/edit"])

                @includeIf("admin.components.buttons.delete",["message" => "" ,  "action" => url("admin/books/$item->id")])

            </td>
        </tr>
    @endforeach
@endsection

@section("filters")
    <form method="get" action="{{url("/admin/books/")}}">

        <div style="display: flex">
            <div class="col-md-3">
                <input type="text" class="form-control name_input " name="bookName" value="{{request()->bookName}}"
                       placeholder="{{trans('language.name')}}">
            </div>
            <div class="col-md-3">
                <select required required class="form-control main_category_id_list main_category_id" id="main_category_id" name="category_id">
                    <option value="0">{{trans('language.category')}}</option>
                @foreach(\App\Models\Category::all() as $category)
                        <option value="{{$category->id}}">{{$category->categoryName}}</option>
                    @endforeach
                </select>
{{--
                @includeIf('admin.components.form.add.select', ['label' => trans("language.country"),'name'=>'country_id', 'items'=> \App\Models\Category::all() , 'icon' => 'fa fa-list',])
--}}

            </div>
            <div class="col-md-3">
                <div class="form-group  hide category_List">
                    <select id="category_id" name="subCategoryId"
                            class=" form-control category_id_list category_id">
                        <option name="subCategoryId" class="basicOption" value="0">اختر القسم الفرعى</option>
                    </select>
                </div>
            </div>

            <div class="col-md-3">
                <input style="width: 45%" type="submit" class="btn btn-success " value="{{trans('language.filter')}}">
                <button style="width: 45%" type="button"
                        class="btn btn-info  reset_inputs ">{{trans('language.reset')}}</button>
            </div>
        </div>
    </form>
@stop

@section("extra_js")

    <script>
        $('.reset_inputs').click(function () {
            $('.name_input').val('');

        });
    </script>

    <script>
        $(document).ready(function () {
            $('.main_category_id').on('change', function () {
                var main_category_id = $('.main_category_id').val();
                $.post("{{url("/getCategories")}}",
                    {
                        category_id: main_category_id,
                        _token: "{{csrf_token()}}"
                    },
                    function (data, status) {

                        if (data.status == true) {
                            $(".category_List").removeClass("hide");
                            $('.category_id_list').html('');
                            if (data.status == true) {
                                $(".category_id_list").prepend('' +
                                    '<option  value="0"> اختر قسم فرعي</option>');
                            }
                            $.each(data, function () {
                                $.each(this, function (index, item) {
                                    console.log(item);
                                    $(".category_id_list").prepend('' +
                                        '<option name="subCategoryId"  value="' + item.id + '">' + item.subCategoryName + '</option>');
                                });
                            });
                        } else {
                            Swal.fire({
                                icon: 'info',
                                text: 'لا توجد اقسام فرعي متفرعه من هذا القسم  '
                            })
                            $('.category_id_list').html('');
                            $(".category_id_list").prepend('' +
                                '<option name="category_id"  value="0">لا توجد اقسام فرعيه </option>');
                        }
                    });
            });
            $('.category_id').on('change', function () {
                var category_id = $('.category_id').val();
                $.post("{{url("/getCategories")}}",
                    {
                        category_id: category_id,
                        _token: "{{csrf_token()}}"
                    },
                    function (data, status) {
                        if (data.status == true) {
                            console.log(data)
                            $('.category_id_list').html('');
                            if (data.status == true) {
                                $(".category_id_list").prepend('' +
                                    '<option name="category_id"  class="basicOption"  value="0"> اختر قسم فرعي</option>');
                            }
                            $.each(data, function () {
                                $.each(this, function (index, item) {
                                    $(".category_id_list").prepend('' +
                                        '<option name="category_id"  value="' + item.id + '">' + item.id + '</option>');
                                });
                            });
                        } else {
                            $('.basicOption').addClass('hide');
                            Swal.fire({
                                icon: 'info',
                                text: 'لا توجد اقسام فرعي متفرعه من هذا القسم  '
                            })
                        }
                    });
            });

        });

    </script>
    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="{{asset("assets/admin/")}}/js/tinymce/tinymce.min.js"></script>
    <script>
        $(document).ready(function () {

            if ($("#mymce").length > 0) {
                tinymce.init({
                    selector: "textarea#mymce",
                    theme: "modern",
                    height: 300,
                    plugins: [
                        "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                        "save table contextmenu directionality emoticons template paste textcolor"
                    ],
                    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",

                });
            }
        });
    </script>

@endsection

