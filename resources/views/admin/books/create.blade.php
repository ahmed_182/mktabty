@extends('admin.layout.forms.add.index')
@section('action' , "books")
@section('title' , trans('language.add'))
@section('page-title',trans('language.books'))
@section('form-groups')
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.name'),'name'=>'bookName', 'placeholder'=>trans('language.name_ar'),'valid'=>trans('language.vaildation')])

    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{trans('language.description')}}</h4>
                <textarea id="bookDescription" name="bookDescription"></textarea>
            </div>
        </div>
    </div>
    <h5> #{{trans('language.categories')}}</h5>
    <br>
    <div class="form-group">
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class='fa fa-list'></i></span>
            </div>
            <select required class="form-control main_category_id_list main_category_id" id="main_category_id"
                    name="categoryId">
                <option value="0">اختر القسم</option>
                @foreach(\App\Models\Category::all() as $category)
                    <option name="main_category_id" value="{{$category->id}}">{{$category->categoryName}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group  hide category_List">
        <select id="category_id" name="subCategoryId"
                class=" form-control category_id_list category_id">
            <option name="subCategoryId" class="basicOption" value="0">اختر القسم</option>
        </select>
    </div>


    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.auther'),'name'=>'autherName', 'placeholder'=>trans('language.auther'),'valid'=>trans('language.vaildation')])

    @includeIf('admin.components.form.add.file', ['icon' => 'fa fa-check','label' => trans('language.image'),'name'=>'imgURL', 'max'=>'2'])
    @includeIf('admin.components.form.add.file', ['icon' => 'fa fa-check','label' => trans('language.bookURL'),'name'=>'bookURL', 'max'=>'2'])

{{--
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.bookURL'),'name'=>'bookURL', 'placeholder'=>trans('language.bookURL'),'valid'=>trans('language.vaildation')])
--}}

@endsection
@section('submit-button-title' ,trans('language.add'))
@section('extra_js')


    <script>
        $(document).ready(function () {
            $('.main_category_id').on('change', function () {
                var main_category_id = $('.main_category_id').val();
                $.post("{{url("/getCategories")}}",
                    {
                        category_id: main_category_id,
                        _token: "{{csrf_token()}}"
                    },
                    function (data, status) {

                        if (data.status == true) {
                            $(".category_List").removeClass("hide");
                            $('.category_id_list').html('');
                            if (data.status == true) {
                                $(".category_id_list").prepend('' +
                                    '<option  value="0"> اختر قسم فرعي</option>');
                            }
                            $.each(data, function () {
                                $.each(this, function (index, item) {
                                    console.log(item);
                                    $(".category_id_list").prepend('' +
                                        '<option name="subCategoryId"  value="' + item.id + '">' + item.subCategoryName + '</option>');
                                });
                            });
                        } else {
                            Swal.fire({
                                icon: 'info',
                                text: 'لا توجد اقسام فرعي متفرعه من هذا القسم  '
                            })
                            $('.category_id_list').html('');
                            $(".category_id_list").prepend('' +
                                '<option name="category_id"  value="0">لا توجد اقسام فرعيه </option>');
                        }
                    });
            });
            $('.category_id').on('change', function () {
                var category_id = $('.category_id').val();
                $.post("{{url("/getCategories")}}",
                    {
                        category_id: category_id,
                        _token: "{{csrf_token()}}"
                    },
                    function (data, status) {
                        if (data.status == true) {
                            console.log(data)
                            $('.category_id_list').html('');
                            if (data.status == true) {
                                $(".category_id_list").prepend('' +
                                    '<option name="category_id"  class="basicOption"  value="0"> اختر قسم فرعي</option>');
                            }
                            $.each(data, function () {
                                $.each(this, function (index, item) {
                                    $(".category_id_list").prepend('' +
                                        '<option name="category_id"  value="' + item.id + '">' + item.id + '</option>');
                                });
                            });
                        } else {
                            $('.basicOption').addClass('hide');
                            Swal.fire({
                                icon: 'info',
                                text: 'لا توجد اقسام فرعي متفرعه من هذا القسم  '
                            })
                        }
                    });
            });

        });

    </script>
    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="{{asset("assets/admin/")}}/js/tinymce/tinymce.min.js"></script>
    <script>
        $(document).ready(function () {

            if ($("#mymce").length > 0) {
                tinymce.init({
                    selector: "textarea#mymce",
                    theme: "modern",
                    height: 300,
                    plugins: [
                        "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                        "save table contextmenu directionality emoticons template paste textcolor"
                    ],
                    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",

                });
            }
        });
    </script>


    <script>
        $(document).ready(function () {


            $('#bookDescription').summernote();

        });

    </script>




@endsection
