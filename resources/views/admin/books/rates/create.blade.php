@extends('admin.layout.forms.add.index')
@section('action' , "categories/$category->id/subcategory")
@section('title' , trans('language.add'))
@section('page-title',trans('language.categories'))
@section('form-groups')
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.name_ar'),'name'=>'subCategoryName', 'placeholder'=>trans('language.name_ar'),'valid'=>trans('language.vaildation')])


@endsection
@section('submit-button-title' ,trans('web.add'))
