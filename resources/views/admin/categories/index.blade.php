@extends('admin.layout.table.index')
@section('page-title',trans('language.categories'))
@section('buttons')

@stop
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.categories')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.category')}}</th>
    <th>{{trans('language.sub_categories')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->categoryName}}</td>
            <td>

                <a href="{{url("admin/categories/$item->id/subcategory/")}}">{{$item->dash_name}}</a>
                @includeIf("admin.components.buttons.custom" , ["href" => "categories/$item->id/subcategory", 'class' => 'bi bi-bell' , 'title'=> trans('language.sub_categories'), 'feather' => 'grid'])

            </td>
            <td>
                @includeIf("admin.components.buttons.edit" , ["href" => "categories/$item->id/edit"])

                @includeIf("admin.components.buttons.delete",["message" => "" ,  "action" => url("admin/categories/$item->id")])

            </td>
        </tr>
    @endforeach
@endsection

@section("filters")
    <form method="get" action="{{url("/admin/categories/")}}">

        <div style="display: flex">
            <div class="col-md-3">
                <input type="text" class="form-control name_input " name="categoryName" value="{{request()->categoryName}}"
                       placeholder="{{trans('language.category')}}">
            </div>

            <div class="col-md-3">
                <input style="width: 45%" type="submit" class="btn btn-success " value="{{trans('language.filter')}}">
                <button style="width: 45%" type="button"
                        class="btn btn-info  reset_inputs ">{{trans('language.reset')}}</button>
            </div>
        </div>
    </form>
@stop

@section("extra_js")

    <script>
        $('.reset_inputs').click(function () {
            $('.name_input').val('');

        });
    </script>

@endsection
