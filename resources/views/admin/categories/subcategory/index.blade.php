@extends('admin.layout.table.index')
@section('page-title',trans('language.subCategories'))
@section('buttons')
    <div style="display: flex">
        <div class="col-md-3">
            @includeIf("admin.components.buttons.addbtn" , ["href" => "subcategory/create",'class' => 'btn btn-success' , 'title'=> trans('web.add'), ])
        </div>

    </div>
@stop
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.sub_categories')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.category')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->subCategoryName}}</td>
            <td>
                @includeIf("admin.components.buttons.edit" , ["href" => "categories/$category_id/subcategory/$item->id/edit"])


                @includeIf("admin.components.buttons.delete",["message" => "" ,  "action" => url("admin/categories/$category_id/subcategory/$item->id")])


            </td>
        </tr>
    @endforeach
@endsection


