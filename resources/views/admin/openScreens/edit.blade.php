@extends('admin.layout.forms.edit.index')
@section('action' , "openScreens/$item->id")
@section('title' , trans('language.edit'))
@section('page-title',trans('language.openScreens'))
@section('form-groups')

    @includeIf('admin.components.form.edit.file', ['icon' => 'fa fa-check','label' => trans('language.image'),'name'=>'image', 'max'=>'2'])

    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.title_ar'),'name'=>'title_ar', 'placeholder'=>trans('language.title_ar'),'valid'=>trans('language.vaildation')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.title_en'),'name'=>'title_en', 'placeholder'=>trans('language.title_en'),'valid'=>trans('language.vaildation')])

    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.text_ar'),'name'=>'text_ar', 'placeholder'=>trans('language.text_ar'),'valid'=>trans('language.vaildation')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.text_en'),'name'=>'text_en', 'placeholder'=>trans('language.text_en'),'valid'=>trans('language.vaildation')])

@endsection
@section('submit-button-title' , trans('language.edit'))
