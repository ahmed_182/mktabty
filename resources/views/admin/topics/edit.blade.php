@extends('admin.layout.forms.edit.index')
@section('action' , "topics/$item->id")
@section('title' , trans('language.edit'))
@section('page-title',trans('language.topics'))
@section('form-groups')

    @includeIf('admin.components.form.edit.file', ['icon' => 'fa fa-check','label' => trans('language.image'),'name'=>'image', 'max'=>'2'])

    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.name_ar'),'name'=>'name_ar', 'placeholder'=>trans('language.name_ar'),'valid'=>trans('language.vaildation')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.name_en'),'name'=>'name_en', 'placeholder'=>trans('language.name_en'),'valid'=>trans('language.vaildation')])@endsection
@section('submit-button-title' , trans('language.edit'))
