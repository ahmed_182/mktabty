@extends('admin.layout.index')
@section('content')


    @php

        $books =\App\Models\Book::where("fileTypeId", 1)->get();
        $audio_books =\App\Models\Book::where("fileTypeId", 2)->get();
               $categories =\App\Models\Category::get();
               $requested_book =\App\Models\RequestedBook::get();
               $all_users =\App\Models\User::where('user_type_id', App\ModulesConst\UserTyps::user)->get();



    @endphp



    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            <h4 class="mb-3 mb-md-0">{{trans('language.welcome_msg')}}</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-12 col-xl-12 stretch-card">
            <div class="row flex-grow">
                <div class="col-md-3 grid-margin stretch-card">
                    <div class="card">
                        <a href="{{url("/admin/countries")}}">
                            <div class="card-body">
                                <div class="d-flex justify-content-between align-items-baseline">
                                    <h6 class="card-title mb-0">{{trans('language.users')}}</h6>
                                    <div class="dropdown mb-2"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-md-12 col-xl-5">
                                        <h3 class="mb-2">{{count($all_users)}}</h3>
                                        <div class="d-flex align-items-baseline">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-12 col-xl-7">
                                        <div id="" class="mt-md-3 mt-xl-0"></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>

                </div>

                <div class="col-md-3 grid-margin stretch-card">
                    <div class="card">
                        <a href="{{url("/admin/categories")}}">
                            <div class="card-body">
                                <div class="d-flex justify-content-between align-items-baseline">
                                    <h6 class="card-title mb-0">{{trans('language.categories')}}</h6>
                                    <div class="dropdown mb-2"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-md-12 col-xl-5">
                                        <h3 class="mb-2">{{count($categories)}}</h3>
                                        <div class="d-flex align-items-baseline">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-12 col-xl-7">
                                        <div id="" class="mt-md-3 mt-xl-0"></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-3 grid-margin stretch-card">
                    <div class="card">
                        <a href="{{url("/admin/books")}}">
                            <div class="card-body">
                                <div class="d-flex justify-content-between align-items-baseline">
                                    <h6 class="card-title mb-0">{{trans('language.books')}}</h6>
                                    <div class="dropdown mb-2"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-md-12 col-xl-5">
                                        <h3 class="mb-2">{{count($books)}}</h3>
                                        <div class="d-flex align-items-baseline">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-12 col-xl-7">
                                        <div id="" class="mt-md-3 mt-xl-0"></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-3 grid-margin stretch-card">
                    <div class="card">

                        <a href="{{url("/admin/audio_books")}}">
                            <div class="card-body">
                                <div class="d-flex justify-content-between align-items-baseline">
                                    <h6 class="card-title mb-0">{{trans('language.audio_books')}}</h6>
                                    <div class="dropdown mb-2"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-md-12 col-xl-5">
                                        <h3 class="mb-2">{{count($audio_books)}}</h3>
                                        <div class="d-flex align-items-baseline">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-12 col-xl-7">
                                        <div id="" class="mt-md-3 mt-xl-0"></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>


            </div>
        </div>

    </div>



    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            <h4 class="mb-3 mb-md-0"># {{trans('language.latestOrders')}}</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-xl-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>{{trans('language.client')}}</th>
                                <th>{{trans('language.mobile')}}</th>
                                <th>{{trans('language.book_name')}}</th>
                                <th>{{trans('language.auther')}}</th>
                                <th>{{trans('language.status')}}</th>
                                <th>{{trans('language.settings')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{$item->userName}}</td>
                                    <td>{{$item->phone}}</td>
                                    <td>{{$item->bookName}}</td>
                                    <td>{{$item->author}}</td>
                                    <td>{{$item->dash_StatusName}}</td>
                                    {{--
                                                <td>{{$item->created_at}}</td>
                                    --}}
                                    <td>
                                        @includeIf("admin.components.buttons.custom" , ["href" => "requested-books/$item->id/edit", 'class' => 'btn btn-warning' , 'title'=> trans('language.status'), 'feather' => 'plus-circle'])
                                        @includeIf("admin.components.buttons.delete",["message" => ($item->name) ,  "action" => url("admin/requested_books/$item->id")])
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section("extra_js")
@endsection
