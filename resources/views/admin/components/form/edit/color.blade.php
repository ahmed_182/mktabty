<div class="form-group">
    <label for="exampleInputuname">{{$label}}</label>
    <div class="input-group">
        <div class="input-group-prepend">
        </div>
        <input name="{{$name}}"
               style="height: 200px;"
               value="{{$item["$name"]}}"
               placeholder="{{$placeholder}}"
               type="color" class="form-control">
    </div>
</div>
