<a href="javascript:void(0)"
   class=" showButton"
   data-original-title="Show"
   data-message="{{$message}}"
   data-toggle="modal"
   data-target=".showModal"
   title="{{trans('language.details')}}">
{{--
    {{trans('language.details')}}
--}}
    <i class="fa fa-eye" data-feather="eye" aria-hidden="true"></i>

</a>
