<a href="javascript:void(0)"
   class="btn btn-danger deleteButton"
   data-original-title="Delete"
   data-message="{{$message}}"
   data-action="{{$action}}"
   data-toggle="modal"
   data-target=".deleteModal"
    title="{{trans('language.delete')}}">
    <i class="fa fa-trash" data-feather="delete" aria-hidden="true"></i>

</a>
