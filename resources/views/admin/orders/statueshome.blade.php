@extends('admin.layout.forms.edit.index')
@section('action' , "orders/$item->id/statuesStore")
@section('title' , trans('language.update_order_status'))
@section('page-title',trans('language.update_order_status'))
@section('form-groups')

    @includeIf('admin.components.form.edit.select', ['label' => trans("language.order_status"),'name'=>'order_status_id', 'items'=> $items , 'icon' => 'fa fa-list',])


@endsection
@section('extra_table')
    <div class="col-lg-12 col-xl-12 stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover mb-0">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{trans('language.name')}}</th>
                            <th>{{trans('language.settings')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($order_status as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{$item->dash_name}}</td>
                                <td>
                                    @includeIf("admin.components.buttons.edit" , ["href" => "order_status/$item->id/edit"])
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('submit-button-title' , trans('web.add'))
