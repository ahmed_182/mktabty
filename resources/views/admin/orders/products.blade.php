@extends('admin.layout.table.index')
@section('page-title',trans('language.products'))
@section('nav')
    <div class="row">
        <div class="col-md-10">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.appName')}}</a></li>
                <li class="breadcrumb-item"><a href="{{url("admin/orders")}}">  {{trans('language.orders')}}</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{trans('language.products')}}</li>

            </ol>
        </div>
    </div>

@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.name')}}</th>
    <th>{{trans('language.count')}}</th>
    <th>{{trans('language.piece_price')}}</th>
    <th>{{trans('language.total_price')}}</th>
    <th>{{trans('language.description')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{@$item->product->name}}</td>
            <td>{{$item->count}}</td>
            <td>{{$item->piece_price}} {{trans('language.app_currency')}} </td>
            <td>{{$item->piece_price * $item->count}} {{trans('language.app_currency')}}</td>
            <td>{{@$item->product->description}}</td>
        </tr>
    @endforeach
@endsection


