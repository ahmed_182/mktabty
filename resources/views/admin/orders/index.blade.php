@extends('admin.layout.table.index')
@section('page-title',trans('language.orders'))
@section('nav')

    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.orders')}}</li>
    </ol>
@endsection
@section('thead')
    <th>{{trans('language.order_number')}}</th>
    <th>{{trans('language.date')}}</th>
    <th>{{trans('language.paymenttype')}}</th>
    <th>{{trans('language.total_price')}}</th>
    <th>{{trans('language.added_tax')}}</th>
    <th>{{trans('language.address')}}</th>
    <th>{{trans('language.status')}}</th>
    <th>{{trans('language.products')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{date('d/m/Y', $item->created_at)}}</td>
            <td>{{@$item->payment_type->dash_name}}</td>
            <td>{{$item->total_cost}} {{trans('language.app_currency')}}  </td>
            <td>{{$item->tax}}% <br>
                <span> <strong> {{   ($item->total_cost * $item->tax / 100 )  }}  {{trans('language.app_currency')}} </strong>  </span>
            </td>
            <td>
                {{@$item->favourite_place->name}}
                <br>
                <small> {{@$item->favourite_place->address}}</small>
            </td>
            <td>{{@$item->Order_statues->dash_name}}</td>
            <td>
                @includeIf("admin.components.buttons.custom" , ["href" => "order_products/$item->id", 'class' => 'bi bi-bell' , 'title'=> trans('language.show'), 'feather' => 'list'])
            </td>
            <td>
                @includeIf("admin.components.buttons.custom" , ["href" => "orders/$item->id/statues", 'class' => 'btn btn-warning' , 'title'=> trans('language.status'), 'feather' => 'plus-circle'])
                @includeIf("admin.components.buttons.delete",["message" =>  trans('language.order_number') . ' ( ' . $item->id . ' ) '  ,  "action" => url("admin/orders/$item->id")])
            </td>
        </tr>
    @endforeach
@endsection


