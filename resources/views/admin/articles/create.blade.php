@extends('admin.layout.forms.add.index')
@section('action' , "articles")
@section('title' , trans('language.add'))
@section('page-title',trans('language.articles'))
@section('form-groups')
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.name'),'name'=>'title', 'placeholder'=>trans('language.name_ar'),'valid'=>trans('language.vaildation')])

    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{trans('language.body')}}</h4>
                <textarea id="body" name="body"></textarea>
            </div>
        </div>
    </div>


    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.audio_url'),'name'=>'audio_url', 'placeholder'=>trans('language.audio_url'),'valid'=>trans('language.vaildation')])

    @includeIf('admin.components.form.add.file', ['icon' => 'fa fa-check','label' => trans('language.image'),'name'=>'image', 'max'=>'2'])

    @includeIf('admin.components.form.add.select', ['label' => trans("language.books"),'name'=>'book_id', 'items'=> \App\Models\Book::all() , 'icon' => 'fa fa-list',])
    @includeIf('admin.components.form.add.select', ['label' => trans("language.user"),'name'=>'userId', 'items'=> \App\Models\User::all() , 'icon' => 'fa fa-list',])

@endsection
@section('submit-button-title' ,trans('language.add'))
@section('extra_js')


    <!-- wysuhtml5 Plugin JavaScript -->


    <script>
        $(document).ready(function () {


            $('#body').summernote();

        });

    </script>




@endsection
