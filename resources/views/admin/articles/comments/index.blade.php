@extends('admin.layout.table.index')
@section('page-title',trans('language.subCategories'))
@section('buttons')
@stop
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item"><a href="{{url("admin/books")}}">  {{trans('language.articles')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.comments')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.owner_comment')}}</th>
    <th>{{trans('language.comments')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->dash_user}}</td>
            <td>{{$item->comment}}</td>
            <td>


                @includeIf("admin.components.buttons.delete",["message" => "" ,  "action" => url("admin/articles/$article_id/comments/$item->id")])


            </td>
        </tr>
    @endforeach
@endsection


