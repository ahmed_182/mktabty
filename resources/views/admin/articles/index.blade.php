@extends('admin.layout.table.index')
@section('page-title',trans('language.categories'))
@section('buttons')

@stop
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.articles')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.title')}}</th>
    <th>{{trans('language.content_article')}}</th>
    <th>{{trans('language.book_name')}}</th>
    <th>{{trans('language.comments')}}</th>
{{--
    <th>{{trans('language.image')}}</th>
--}}
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->title}}</td>
            <td>
                <p style="display: inline">{!! substr($item->body,0,50) !!}
                </p>  @includeIf("admin.components.buttons.showPop",["message" => $item->body ])

            </td>

            <td>{{$item->dash_book}}</td>
            <td>

                @includeIf("admin.components.buttons.custom" , ["href" => "articles/$item->id/comments", 'class' => 'bi bi-bell' , 'title'=> trans('language.comments'), 'feather' => 'list'])

            </td>
{{--
            <td> @includeIf("admin.components.image.index" , ["url" => $item->image])</td>
--}}

            <td>
                @includeIf("admin.components.buttons.edit" , ["href" => "articles/$item->id/edit"])

                @includeIf("admin.components.buttons.delete",["message" => "" ,  "action" => url("admin/articles/$item->id")])

            </td>
        </tr>
    @endforeach
@endsection

@section("filters")
    <form method="get" action="{{url("/admin/articles/")}}">

        <div style="display: flex">
            <div class="col-md-3">
                <input type="text" class="form-control name_input " name="title" value="{{request()->title}}"
                       placeholder="{{trans('language.title')}}">
            </div>
            <div class="col-md-3">
                <select required class="form-control" name="book_id">
                    <option value="0">{{trans('language.book_name')}}</option>
                    @foreach(\App\Models\Book::all() as $book)
                        <option value="{{$book->id}}">{{$book->bookName}}</option>
                    @endforeach
                </select>
                {{--
                                @includeIf('admin.components.form.add.select', ['label' => trans("language.country"),'name'=>'country_id', 'items'=> \App\Models\Category::all() , 'icon' => 'fa fa-list',])
                --}}

            </div>

            <div class="col-md-3">
                <input style="width: 45%" type="submit" class="btn btn-success " value="{{trans('language.filter')}}">
                <button style="width: 45%" type="button"
                        class="btn btn-info  reset_inputs ">{{trans('language.reset')}}</button>
            </div>
        </div>
    </form>
@stop

@section("extra_js")

    <script>
        $('.reset_inputs').click(function () {
            $('.name_input').val('');

        });
    </script>

@endsection

