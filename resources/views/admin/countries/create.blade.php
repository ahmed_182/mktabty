@extends('admin.layout.forms.add.index')
@section('action' , "countries")
@section('title' , trans('language.add'))
@section('page-title',trans('language.countries'))
@section('form-groups')
    @includeIf('admin.components.form.add.file', ['icon' => 'fa fa-check','label' => trans('language.image'),'name'=>'image', 'max'=>'2'])

    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.name_ar'),'name'=>'name_ar', 'placeholder'=>trans('language.name_ar') ,'valid'=>trans('language.vaildation')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.name_en'),'name'=>'name_en', 'placeholder'=>trans('language.name_en') ,'valid'=>trans('language.vaildation')])
      @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.code'),'name'=>'code', 'placeholder'=>trans('language.code'),'valid'=>trans('language.vaildation')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.number_count'),'name'=>'number_count', 'placeholder'=>trans('language.number_count') ,'valid'=>trans('language.vaildation')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.start_with'),'name'=>'start_with', 'placeholder'=>trans('language.start_with') ,'valid'=>trans('language.vaildation')])


@endsection
@section('submit-button-title' , trans('language.add'))
