@extends('admin.layout.table.index')
@section('page-title',trans('language.countries'))
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.countries')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.image')}}</th>
    <th>{{trans('language.name')}}</th>
{{--    <th>{{trans('language.Currency')}}</th>--}}
    <th>{{trans('language.code')}}</th>
     <th>{{trans('language.start_with')}}</th>
    <th>{{trans('language.number_count')}}</th>

    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td> @includeIf("admin.components.image.index" , ["url" => $item->dash_image])</td>
            <td>{{$item->dash_name}}</td>
{{--            <td>{{$item->curreny_ar}}</td>--}}
            <td>{{$item->code}}</td>
            <td>{{$item->start_with}}</td>

            <td>{{$item->number_count}}</td>

            <td>
                @includeIf("admin.components.buttons.edit" , ["href" => "countries/$item->id/edit"])
                @includeIf("admin.components.buttons.delete",["message" =>  "($item->dash_name)" ,  "action" => url("admin/countries/$item->id")])
{{--
                @includeIf("admin.components.buttons.custom" , ["href" => "countries/$item->id/cities/", 'class' => 'default' , 'title'=> trans('language.cities'), 'icon' => 'fa fa-list-ul','feather' => 'list'])
--}}
            </td>
        </tr>
    @endforeach
@endsection


