@extends('admin.layout.table.index')
@section('page-title',trans('language.start_with'))
@section('buttons')
    @includeIf("admin.components.buttons.addbtn" , ["href" => url("admin/countries/$country_id/start_with/create"),'class' => 'btn btn-success' , 'title'=> trans('web.add'), ])
@stop
@section('thead')
    <th>#</th>
    <th>{{trans('language.start_with')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->start_with}}</td>
            <td>
                @includeIf("admin.components.buttons.edit" , ["href" => "countries/$country_id/start_with/$item->id/edit"])
                @includeIf("admin.components.buttons.delete",["message" =>  "($item->start_with)" ,  "action" => url("admin/countries/$country_id/start_with/$item->id")])
            </td>
        </tr>
    @endforeach
@endsection


