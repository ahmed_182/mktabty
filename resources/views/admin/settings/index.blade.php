@extends('admin.layout.forms.edit.index')
@section('action' , "setting/$item->id")
@section('title' , trans('language.edit'))
@section('page-title',trans('language.settings'))
@section('form-groups')
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.tax'),'name'=>'tax', 'placeholder'=>trans('language.tax')])



@endsection
@section('submit-button-title' , trans('web.edit'))
