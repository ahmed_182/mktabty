<p style='line-height: 1.7em; text-align: justify; font-size: 14px'>
    <b>عزيزنا   , {{$name}} </b>
</p>

<p style='line-height: 1.7em; text-align: justify; font-size: 14px'>
    <b>هذا البريد الإلكتروني لإبلاغك أنك نسيت كلمة المرور الخاصة بك.</b>
  <a href="{{url("/forget/$api_token")}}">{{ __('Send Password Reset Link') }} </a>
</p>
<span style="color:darkblue"> إذا كان لديك أي أسئلة يمكنك التواصل معنا عبر بريدنا الإلكتروني , (mktabty@info.com) </span>
<strong>
    شكرا لك .
</strong>
