<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->integer('id', true);
            $table->text('name')->nullable();
            $table->text('email')->nullable();
            $table->text('password')->nullable();
            $table->text('phone')->nullable();
            $table->text('username')->nullable();
            $table->text('bio')->nullable();
            $table->text('image')->nullable();
            $table->text('facebookid')->nullable();
            $table->text('token')->nullable();
            $table->string('deviceType')->nullable();
            $table->enum('type', ['user', 'admin'])->nullable()->default('user');
            $table->string('fireBaseToken')->nullable();
            $table->string('api_token', 80)
                ->unique()
                ->nullable()
                ->default(null);
            $table->string('remember_token')->nullable();
            $table->string('passCode')->nullable();
            $table->string('social_id')->nullable();
            $table->string('loginBy_id')->nullable();
            $table->string('email_verified_at')->nullable();
            $table->timestamps();
            $table->integer('user_type_id')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
