<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('image')->nullable();
            $table->string('title', 300);
            $table->text('body');
            $table->string('audio_url')->nullable();
            $table->string('book_id')->nullable();
            $table->integer('userId');
            $table->bigInteger('no_views')->nullable();
            $table->bigInteger('no_shares')->nullable();
            $table->bigInteger('no_comments')->nullable();
            $table->bigInteger('no_likes')->nullable();
            $table->bigInteger('no_dislikes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
