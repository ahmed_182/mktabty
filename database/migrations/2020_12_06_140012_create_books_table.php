<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('fileTypeId')->nullable()->default('1');
            $table->string('size')->nullable();
            $table->text('bookName')->nullable();
            $table->longText('bookDescription')->nullable();
            $table->integer('categoryId')->nullable();
            $table->integer('subCategoryId')->nullable();
            $table->text('autherName')->nullable();
            $table->text('reader_name')->nullable();
            $table->text('imgURL')->nullable();
            $table->text('bookURL')->nullable();
            $table->integer('our_pick')->nullable();
            $table->bigInteger('no_views')->nullable();
            $table->bigInteger('no_shares')->nullable();
            $table->bigInteger('no_comments')->nullable();
            $table->bigInteger('no_likes')->nullable();
            $table->bigInteger('no_dislikes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
