<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAskBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ask_books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone', 191)->nullable();
            $table->string('userName', 191)->nullable();
            $table->string('bookName', 191)->nullable();
            $table->string('authorName', 191)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ask_books');
    }
}
