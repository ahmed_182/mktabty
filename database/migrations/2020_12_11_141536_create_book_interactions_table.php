<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookInteractionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_interactions', function (Blueprint $table) {
            $table->id();
            $table->string("user_id");
            $table->string("book_id");
            $table->integer("type"); // 1 for like , 2 for dislike , 3 for shared
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_interactions');
    }
}
