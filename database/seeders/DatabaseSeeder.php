<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(1)->create();
        \App\Models\UserTopic::factory(1000)->create();
        \App\Models\IntroductionMessage::factory(3)->create();
        \App\Models\Book::factory(1000)->create();
        \App\Models\Article::factory(1000)->create();
        \App\Models\BookComment::factory(10000)->create();
        \App\Models\Comment::factory(10000)->create();
        \App\Models\Category::factory(100)->create();
        \App\Models\SubCategory::factory(100)->create();
        \App\Models\Topic::factory(100)->create();
        \App\Models\Download::factory(100)->create();
    }
}
