<?php

namespace Database\Factories;

use App\Models\Topic;
use App\Models\UserTopic;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class TopicFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Topic::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'image' => "https://placeimg.com/200/200/any",
            'name_ar' => Str::random(5),
            'name_en' => Str::random(5),
        ];
    }
}


