<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'bio' => Str::random(200),
            'email' => "dummy@mktabty.com",
            'email_verified_at' => now(),
            'password' => '123456',
            'remember_token' => Str::random(10),
            'api_token' => "i2MAKq3TSF5AymHCkR1jDFoHLAZ7aQUjCLWm9Vvck2lBW1kdIY17X74QLZpL",
        ];
    }
}
