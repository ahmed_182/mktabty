<?php

namespace Database\Factories;

use App\Models\IntroductionMessage;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class IntroductionMessageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = IntroductionMessage::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'image' => "https://placeimg.com/200/200/any",
            'title_ar' => Str::random(20),
            'title_en' => Str::random(20),
            'text_ar' => Str::random(60),
            'text_en' => Str::random(60),
        ];
    }
}
