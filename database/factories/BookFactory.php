<?php

namespace Database\Factories;

use App\Models\Book;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class BookFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Book::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'fileTypeId' => $this->faker->numberBetween(1,2),
            'bookName' => Str::random(10),
            'reader_name' => Str::random(10),
            'bookDescription' => Str::random(60),
            'categoryId' => $this->faker->numberBetween(1,10),
            'subCategoryId' => $this->faker->numberBetween(1,10),
            'autherName' => Str::random(5),
            'no_views' => $this->faker->numberBetween(100,1000),
            'imgURL' => "https://placeimg.com/200/200/any",
            'bookURL' => $this->faker->imageUrl(200,200),
            'our_pick' => $this->faker->numberBetween(0,1),
            'size' => $this->faker->numberBetween(1,50),
            'no_shares' => $this->faker->numberBetween(1,50),
        ];
    }
}
