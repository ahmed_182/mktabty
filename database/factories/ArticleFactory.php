<?php

namespace Database\Factories;

use App\Models\Article;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ArticleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Article::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'image' => "https://placeimg.com/200/200/any",
            'title' => Str::random(5),
            'body' => Str::random(191),
            'audio_url' => $this->faker->imageUrl(200,200),
            'userId' => $this->faker->numberBetween(1,2),
            'book_id' => $this->faker->numberBetween(1,2),
            'no_likes' => $this->faker->numberBetween(1,10),
            'no_shares' => $this->faker->numberBetween(1,10),
            'no_comments' => $this->faker->numberBetween(1,10),
            'no_dislikes' => $this->faker->numberBetween(1,10),
            'no_views' => $this->faker->numberBetween(1,10),
        ];
    }
}
