<?php

namespace Database\Factories;

use App\Models\UserTopic;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserTopicFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UserTopic::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => $this->faker->numberBetween(1,2),
            'topic_id' => $this->faker->numberBetween(1,50),
        ];
    }
}
