<?php

namespace Database\Factories;

use App\Models\BookComment;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use phpDocumentor\Reflection\Types\Integer;

class BookCommentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BookComment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => $this->faker->numberBetween(1,2),
            'book_id' => $this->faker->numberBetween(1,2),
            'comment' => Str::random(10),
            'rate' => $this->faker->numberBetween(1, 5),
        ];
    }
}
